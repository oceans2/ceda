<?php include('config.php'); 
 include('header.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();
$regid =$_REQUEST['regid'];
$sql="SELECT * 
FROM  `schloarshipexamreg` 
LEFT JOIN exam ON exam.Id = schloarshipexamreg.ExamName
WHERE schloarshipexamreg.Reg_Id = '$regid' AND schloarshipexamreg.Marks";
$rmName = $db->ExecuteQuery($sql);

?>
<style>
@media print
{
header, .topStrip, .no-print {display:none;}
}
</style>

<div class="main">
  
  <div style="border-bottom:solid 1px #000;">
    <div class="col-sm-2" style="float:left"><img width="115" src="images/logo.png" alt=""></div>
    <div class="col-sm-10 text-center" style="float:left">
      <h2 style="margin-top:0;"><strong>RESULT<br>SCHOLARSHIP TEST EXAM</strong></h2>
      <p style="font-size:1.5em; font-weight:bold;"> RICEs (Rural Institute for Career & Employment Society)<br>
        AN ISO 9001:2015 CERTIFIED ORGANIZATION<br>
        (Reg. No. 4376)</p>
    </div>
    <div class="clearfix">&nbsp;</div>
  </div>

  <div style="margin-top:30px;">
    <div class="col-sm-10" style="width:80%; float:left;">
      

      <table width="100%" cellpadding="5">
        <tr>
          <td width="150" class="bg-success">Date :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo date("d-m-Y"); ?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Student’s Name :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $rmName[1]['Student_Name']; ?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Father's Name :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $rmName[1]['Father_Name']; ?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Mother's Name  :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $rmName[1]['Father_Name']; ?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Roll:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $rmName[1]['Reg_Id']; ?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Exam Center Name:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $rmName[1]['Centername'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Exam Center Code:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $rmName[1]['Centercode'];?></td>
        </tr>
      </table>


    </div>
    <div class="col-sm-2" style="width:20%; float:left;">
      <div class="ef_header_tools pull-right"><img src="<?php echo PATH_DATA_IMAGE."/scholar/thumb/".$rmName[1]['Photo']; ?>" width="100%" alt="Candidate photo"></div>
    </div>
    <div class="clearfix"></div>

</div>



 <h2 class="text-center">RESULT DECLARATION</h2>
 <table width="100%" border="1" cellspacing="12" cellpadding="12">
   <tr>
    <th scope="row">Total Marks</th>
    <th scope="row">Obtain Marks</th>
    <th scope="row">Result</th>
    <th scope="row">Division Grade </th>
    <th scope="row">Percent </th>    
  </tr>

  <tr>
    <td>100</td>
    <td><?php echo $rmName[1]['Marks']; ?></td>
    <td>
    <?php if($rmName[1]['Marks']<33){echo "Fail";}else{echo "Pass";} ?></td>
    <td>
      <?php if($rmName[1]['Marks']>=33 && $rmName[1]['Marks']<=44){echo '2nd';}
      if($rmName[1]['Marks']>=45 && $rmName[1]['Marks']<=59){echo '1st';}
      if($rmName[1]['Marks']>=60 && $rmName[1]['Marks']<=69){echo 'C';}
      if($rmName[1]['Marks']>=70 && $rmName[1]['Marks']<=84){echo 'B';}
      if($rmName[1]['Marks']>=85 && $rmName[1]['Marks']<=100){echo 'A';}
      ?>
    </td>
    <td><?php echo $rmName[1]['Marks'];?>%</td>
  </tr>
 </table>
    
  <div class="no-print" style="text-align:center; margin-top:40px;">
  	<form>
          <input type="button" onClick="window.print()" id="submit" name="submit" value="Print"/>
    </form>
  </div>

  <div style="margin-top:50px;">
<strong>निर्देश</strong>
  <ul>
      <li>1.  प्राप्त अंक का विवरण ,32 मार्क – fail ,33 मार्क से 44 सेकंड डिविजन , 45 मार्क से 59 मार्क – फ़र्स्ट डिविजन , 60 से 69 मार्क ग्रेड “सी” , 70 से 84 मार्क ग्रेड “बी” , 85 से 100 मार्क ग्रेड “ए” मान्य है </li>
      <li>2.  प्राप्त रिज़ल्ट मे रीचेकिंग या रिकाउंट अप्लाई के लिए, रिज़ल्ट घोषणा के तीन दिन तक मान्य होगा इसके लिए ऑफलाइन ,रीचेकिंग या रिकाउंट फार्म download करके उसे फ़िल करके 50/- फीस रसीद के साथ exam@riceedu.org पर मेल करें रिज़ल्ट दों दिन बाद  घोषणा  किया जाएगा |</li>
  </ul>
</div>
       