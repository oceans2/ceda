<?php 
include('config.php');
require_once(PATH_LIBRARIES.'/classes/DBConn.php');

$db = new DBConn();
$pagename = basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);

$path = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$file2 = basename($path, ".php"); // $file2 is set to "article-list"
?>
<!DOCTYPE HTML>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <title>SDMS Online</title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap-theme.min.css" rel="stylesheet"  type="text/css">
    <link href="css/style.css" rel="stylesheet"  type="text/css"/>
    <link href="css/sub_menu.css" rel="stylesheet" type="text/css"/>
	<link rel="stylesheet" href="css/font-awesome.min.css">
    
	<script src="<?php echo PATH_JS_LIBRARIES; ?>/jquery.js" type="text/javascript"></script>
    <script src="<?php echo PATH_JS_LIBRARIES; ?>/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo PATH_JS_LIBRARIES; ?>/jquery.slides.min.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES; ?>/jquery.validate.js"></script>
    
</head>
<body>
	<div class="containerTopBor container-fluid">
    	<!--header contaner start here-->
        <header>
        	<hgroup>
            	<h1 class="site-title">SAMRIDDHI PARISAR</h1>
                <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <nav class="site-navigation collapse navbar-collapse bs-navbar-collapse">
                	<ul>
                    	<li><a <?php if ($file2 == 'samriddhi.suncrossonline.com' || $pagename == 'index.php'){?>class="active"<?php } ?> href="index.php">HOME</a></li>
                        
                        
						<?php // get the first project						
						$proLayoutNav=$db->ExecuteQuery("SELECT Project_Id FROM project_master ORDER BY Project_Name ASC LIMIT 1"); ?>
                        <li><a <?php if ($pagename == 'project.php' || $pagename == 'project_category.php' || $pagename == 'project_subcategory.php'){?>class="active"<?php } ?> href="project.php?pid=<?php echo $proLayoutNav[1]['Project_Id']; ?>">PROJECT LAYOUTS</a></li>
                        
                        
                        <li><a <?php if ($pagename == 'facilities.php'){?>class="active"<?php } ?> href="facilities.php">FACILITIES</a></li>
                        
                        
						<?php // get the first gallery images
						$galleryNav=$db->ExecuteQuery("SELECT Project_Id FROM gallery 
						ORDER BY Gallery_Id ASC LIMIT 1");
						?>
                        
                        <li><a <?php if ($pagename == 'gallery.php'){?>class="active"<?php } ?> href="gallery.php?pid=<?php echo $galleryNav[1]['Project_Id'] ?>">GALLERY</a></li>
                        
                        
                        <li><a <?php if ($pagename == 'contact-us.php'){?>class="active"<?php } ?> href="contact-us.php">CONTACT US</a></li>
                    </ul>
                </nav>
                <div class="clearfix"></div>
            </hgroup>
        </header>
        <!--header contaner end here ** top-slider contaner start here-->