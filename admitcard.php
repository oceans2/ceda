<?php 
include('config.php'); 
include('header.php');
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();
$regid =$_REQUEST['regid'];
$sql="SELECT *, DATE_FORMAT(Date,'%d-%m-%Y') AS Exam_Date FROM  `schloarshipexamreg` 
LEFT JOIN exam ON exam.Id = schloarshipexamreg.ExamName
WHERE schloarshipexamreg.Reg_Id = '$regid'";
$rmName = $db->ExecuteQuery($sql);
?>
  
<style>
@media print
{
header, .topStrip, .no-print {display:none;}
}
</style>

<div class="main">
  
  <div style="border-bottom:solid 1px #000;">
    <div class="col-sm-2" style="float:left"><img width="115" src="images/logo.png" alt=""></div>
    <div class="col-sm-10 text-center" style="float:left">
      <h2 style="margin-top:0;"><strong>ADMIT CARD<br>SCHOLARSHIP TEST EXAM</strong></h2>
      <p style="font-size:1.5em; font-weight:bold;"> RICEs (Rural Institute for Career & Employment Society)<br>
        AN ISO 9001:2015 CERTIFIED ORGANIZATION<br>
        (Reg. No. 4376)</p>
    </div>
    <div class="clearfix">&nbsp;</div>
  </div>

  <div style="margin-top:30px;">
    <div class="col-sm-10" style="width:80%; float:left;">
    <?php 
                $i=1;
                foreach($rmName as $getDistrictVal){ ?>
      <table width="100%" cellpadding="5">
        
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="50">Date</td>
                <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Exam_Date'];?></td>
                <td width="50">Time</td>
                <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Time'];?></td>
              </tr>
            </table>
          </td>          
        </tr>
        <tr>
          <td width="150" class="bg-success">Student’s Name :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Student_Name'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Father's Name :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Father_Name'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Mother's Name  :</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Mother_Name'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Aadhaar Number:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Aadhaar_No'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Contact No:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Contact_No'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Roll:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Reg_Id'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Exam Center Name:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Centername'];?></td>
        </tr>
        <tr>
          <td width="150" class="bg-success">Exam Center Code:</td>
          <td style="border-bottom:solid 1px #666;"><?php echo $getDistrictVal['Centercode'];?></td>
        </tr>
      </table>    
      <?php } ?>
    </div>  
    <div class="col-sm-2" style="width:20%; float:left;">
      <div class="ef_header_tools pull-right"><img src="<?php echo PATH_DATA_IMAGE."/scholar/thumb/".$getDistrictVal['Photo']; ?>" width="100%" alt="Candidate photo"></div>
    </div>
    <div class="clearfix"></div>
  </div>

</div>

  
<div class="no-print" style="text-align:center; margin-top:40px;">
	<form>
        <input type="button" onClick="window.print()" id="submit" name="submit" value="Print"/>
  </form>
</div>
<div style="margin-top:50px;">
	<strong>निर्देश</strong>
    <ul>
    	  <li>1.  एक्जाम सेंटर मे एड्मिट कार्ड अनिवार्य है</li>
        <li>2.  एक्जाम सेंटर मे पहचान पत्र अनिवार्य है</li>
        <li>3.  एक्जाम सेंटर मे मोबाइल फोन ,कैल्कुलेटर, वैद्य नहीं है</li>
        <li>4.  एक्जाम सेंटर मे एक्जाम स्टार्ट होने के 15 मिनट देरी के बाद स्टूडेंट को प्रवेश नहीं दिया जाएगा उसका जिम्मेदार स्टूडेंट होगा </li>
    </ul>
</div>