<?php
include('config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();
include('header.php');

// get all list of states
$getStates=$db->ExecuteQuery("SELECT * FROM state_master ");

// get all list of courses
$getCourse=$db->ExecuteQuery("SELECT * FROM course_master ");
$paymentGateway=$db->ExecuteQuery("SELECT Merchant_Key, Salt_Key FROM `tbl_payment_gateway_detail` WHERE `Status`=1");


/////////////////////////////////////////////////////
// PayUMoney Code	/////////////////////////////////
/////////////////////////////////////////////////////
// Merchant Salt as provided by Payu
$MERCHANT_KEY = $paymentGateway[1]['Merchant_Key'];
// Merchant Salt as provided by Payu
$SALT =  $paymentGateway[1]['Salt_Key'];

// End point - change to https://secure.payu.in for LIVE mode
//$PAYU_BASE_URL = "https://test.payu.in"; //for test 
$PAYU_BASE_URL = "https://secure.payu.in"; // for vanvinodan
$txnid = substr(hash('sha256', mt_rand() . microtime()), 0, 20);
$hash = '';
$getexam=$db->ExecuteQuery("SELECT * FROM exam Where Action!=1");
?>
<script type="text/javascript" src="student.js"></script>
<style>
#loading {
    background: rgba(0,0,0,0.5);
    height: auto;
    width: 100%;
    height: 768px;
    position: fixed;
	left:0;
    z-index: 99;
    display: block;
    top: 0;
}
#loader{background:rgba(0,0,0,0.5); height:auto; width:100%; height: 768px; position:fixed; z-index:99; display:none; top:0;}
.loader-block{widht:50px; margin:0 auto; text-align:center; margin-top:150px;}
.spinloader{font-size:40px !important; position:relative !important; left:0; color:#fff;}
</style>
<div id="loading">
    <div class="loader-block"><i class="fa-li fa fa-spinner fa-spin spinloader"></i></div>
</div>

<div>
  <div class="page-title">
    <div class="title_left">
      <h3><i class="glyphicon glyphicon-plus"></i> Scholar Ship  </h3>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2></h2>
          <ul class="nav navbar-right panel_toolbox">
            <li>
            </li>
          </ul>
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        	<form class="form-horizontal" role="form" id="insertStudent" method="post">
              <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="student_name">Exam Name <span>*</span>:</label>
                  <div class="col-sm-5">
<select name="exam" id="exam" class="form-control input-sm" >
<option value="">-- Select --</option>
<?php foreach($getexam as $getDistrictVal){ ?>
<option value="<?php echo $getDistrictVal['Id']; ?>"><?php echo $getDistrictVal['Examname']; ?></option>
<?php } ?>
</select>
                  </div>
                </div>
                
            
              <div>
                
                
                <div class="form-group">
                    <label class="col-md-4 control-label mandatory" for="fileupload">Upload Student Photo <span>*</span>:</label>
                    <div class="col-md-4">
                        <label class="col-md-4 control-label" for="fileupload"><span class="glyphicon glyphicon-user" style="font-size:50pt;"></span></label>
                        <input class="col-md-8" type="file" id="fileupload" name="fileupload">
                   </div>
                </div>
        
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="student_name">Student Name <span>*</span>:</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="student_name" name="student_name" placeholder="Name of the Student" />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="dob">Date of Birth <span>*</span> </label>
                  <div class="col-sm-2">
                    <input type="text" id="dob" name="dob" required="required" class="form-control datetimepicker" placeholder="DD/MM/YYYY">
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-md-4 control-label" for="gender">Gender</label>
                  <div class="col-md-4"> 
                    <label class="radio-inline" for="gender-0">
                      <input name="gender" id="gender-0" value="1" checked="checked" type="radio">
                      Male
                    </label> 
                    <label class="radio-inline" for="gender-1">
                      <input name="gender" id="gender-1" value="2" type="radio">
                      Female
                    </label>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="father_name">Father's Name <span>*</span>:</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="father_name" name="father_name" placeholder="Father's Name" />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="mother_name">Mother's Name <span>*</span>:</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="mother_name" name="mother_name" placeholder="Mother's Name" />
                  </div>
                </div>
           
                
                <div class="form-group">
                  <label class="col-md-4 control-label mandatory" for="caste">Caste <span>*</span></label>
                  <div class="col-md-3">
                    <select id="caste" name="caste" class="form-control">
                     <option value="">--- Select Caste  ---</option>
                      <option value="GEN">GEN</option>
                      <option value="OBC">OBC</option>
                      <option value="SC">SC</option>
                      <option value="ST">ST</option>
                      <option value="SATNAMI">SATNAMI</option>
                      <option value="MINORITY">MINORITY</option>
                      <option value="JAIN">JAIN</option>
                      <option value="MUSLIM">MUSLIM</option>
                    </select>
                  </div>
                </div>
                
               
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="aadhaar_no">Aadhaar Card No. <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="aadhaar_no" name="aadhaar_no" placeholder="Aadhaar Card No." />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="col-md-4 control-label mandatory" for="education">Education <span>*</span></label>
                  <div class="col-md-3">
                    <select id="education" name="education" class="form-control">
                     <option value="">--- Select Education  ---</option>
                      <option value="8th">8th</option>
                      <option value="10th">10th</option>
                      <option value="12th">12th</option>
                      <option value="Diploma">Diploma</option>
                      <option value="BA">BA</option>
                      <option value="B COM">B COM</option>
                      <option value="BSC">BSC</option>
                      <option value="BCA">BCA</option>
                      <option value="BE">BE</option>
                      <option value="B TECH">B TECH</option>
                      <option value="MA">MA</option>
                      <option value="MBA">MBA</option>
                      <option value="MSC">MSC</option>
                      <option value="MCA">MCA</option>
                      <option value="M TECH">M TECH</option>
                      <option value="GRADUATE">GRADUATE</option>
                      <option value="POST GRADUATE">POST GRADUATE</option>
                    </select>
                  </div>
                </div>
                
                <hr />
                
                <h4>Course Details</h4>
                
                <hr />
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="course">Course <span>*</span>:</label>
                  <div class="col-sm-3">
                    <select id="course" name="course" class="form-control">
                     <option value="">--- Select Course  ---</option>
                    <option value="MR">MR(MEDICAL REPRESENTATIVE)</option>
                    
                    </select>
                  </div>
                </div>
               
                
                <hr />
                
                <h4>Contact Details</h4>
                
                <hr />
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="state">State <span>*</span>:</label>
                  <div class="col-sm-4">
                    <select name="state" id="state" class="form-control" >
                      <option value="">--Select Type--</option>
                      <?php foreach($getStates as $getStatesVal){ ?>
                      <option value="<?php echo $getStatesVal['State_Id']; ?>"><?php echo $getStatesVal['State_Name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="district">District <span>*</span>:</label>
                  <div class="col-sm-3">
                    <select name="district" id="district" class="form-control" >
                      <option value="">--Select Type--</option>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="block">Block <span>*</span>:</label>
                  <div class="col-sm-3">
                    <select name="block" id="block" class="form-control" >
                      <option value="">--Select Type--</option>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="address">Address <span>*</span>:</label>
                  <div class="col-sm-4">
                    <textarea  class="form-control txtarea" id="address" name="address" placeholder="Address"></textarea>
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="pincode">Pin Code <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="pincode" name="pincode" placeholder="Ex: 490020" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="pincode">Post Office <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="postofice" name="postofice" placeholder="Postofice" />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="contact_no">Contact No <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="contact_no" name="contact_no" placeholder="Contact No" />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="email">Email <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control" id="email" name="email" placeholder="Email" />
                     <?php  
                                       
                                  ?>
                    <textarea name="productinfo" id="productinfo" hidden></textarea><br />
                     <input type="hidden" name="key" id="key" value="<?php echo $MERCHANT_KEY; ?>" />
                                  <input type="hidden" id="hash" name="hash" value="<?php echo $hash; ?>"/>
                                  <input type="hidden" id="txnid" name="txnid" value="<?php echo $txnid;?>" />
             <input type="hidden" id="amount" name="amount" value="300" /><br />
                <input name="surl"  id="surl" type="hidden" value="<?php echo $surl; ?>" />
                <input name="furl"  id="furl"  type="hidden"  value="<?php echo $furl; ?>" />
                <input type="hidden" name="service_provider"  id="service_provider"  value="payu_paisa" size="64" />
               <input type="hidden" name="firstname" id="firstname"  value="" />

                  </div>
                </div>
                
                <hr />
                
          

                <hr />
                
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-3">
                    <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
                    <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
                  </div>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
  
  
</div>
<?php require_once(PATH_CM_INCLUDE.'/footer.php'); ?>