
        <!-- footer content -->
        <footer>
          <div class="pull-right">
          	RURAL INSTITUTE FOR CAREER AND EMPLOYMENT SOCIETY (RICEs)
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
      <!-- /page content -->
    </div>

</div>

  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/bootstrap.min.js"></script>

  <!-- bootstrap progress js -->
  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/progressbar/bootstrap-progressbar.min.js"></script>
  <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES ?>/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES ?>/js/bootstrap-datetimepicker.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/custom.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/jquery.validate.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/tiny_mce/tiny_mce.js"></script>
   <script type="text/javascript">
   <!--Time Picker Show here -->
	$(function () {
		$('.datetimepicker').datepicker({
			orientation: "top auto",
			forceParse: false,
			autoclose: true,
			dateFormat: 'dd-mm-yy'
		});
	});
	<!--Datepicker Show here-->
 </script>

</body>

</html>
