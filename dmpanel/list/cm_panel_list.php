<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();
include(PATH_DM_INCLUDE.'/header.php');
//print_r($_SESSION);die;

//$sql=$db->ExecuteQuery("SELECT * FROM cm_login WHERE Center_Code='".$_POST['center_Code']."'");
$sql_dm=$db->ExecuteQuery("select DM_Emp_Code from dm_login where DM_Id='".$_SESSION['dmid']."'");

//$sql=$db->ExecuteQuery("SELECT Center_Code FROM cm_login where DM_Emp_Code  ='".$sql_dm[1]['DM_Emp_Code']."'");

$sql=$db->ExecuteQuery
("SELECT Emp_Name, CM_Emp_Code,Center_Code,CM_Contact_No FROM cm_login as cl INNER JOIN employee_master as em ON cl.CM_Emp_Code = em.EMP_Code AND DM_Emp_Code='".$sql_dm[1]['DM_Emp_Code']."'");
?>

<script>
$(document).ready(function()
{
           var formdata = new FormData();
      formdata.append('type', "all");
$.ajax({
         type: "POST",
         url: "list_curd.php",
         data:formdata,
         success: function(data){ //alert(data);
          
          if(data)
          {
            $('#displayInfo').html(data);
          }
          
        },
         cache: false,
         contentType: false,
         processData: false
      });//eof ajax  


});
</script>
<script type="text/javascript" src="list.js"></script>

<div id="loading">
    <div class="loader-block"><i class="fa-li fa fa-spinner fa-spin spinloader"></i></div>
</div>

<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

<div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Center List</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Center List</li>
          </ol>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>
    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="x_panel">
        <div class="x_title">
          <!-- <h2>Search Center</h2> -->
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
          <!-- <form class="form-horizontal" role="form" id="findcmFrm" method="post">
              <div>
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="center_Code">Center Code <span>*</span>:</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="center_Code" name="center_Code" placeholder="Center Code" />
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-sm" id="search"><i class="fa fa-search"></i> Search</button>
                  </div>
                </div>
                
                <hr />
                
              </div>
            </form> -->

            <div class="card">
              <div class="card-body">
                <h4 class="card-title">List of Center Managers</h4>
                <!-- <h6 class="card-subtitle">Data table example</h6> -->
                <div class="table-responsive m-t-40">
                  <div id="cmlist">
                    <table id="cmlisttable" class="table table-bordered table-striped">
                      <thead>
                        <tr class="success">
                          <th>Sno.</th>
                          <th>CM Code</th>
                          <th>CM Name</th>
                          <th>CM Contact Number</th>
                          <th>CM Center Code</th>
                          <th>Total Student Count</th>
                        </tr>
                        

                      </thead>
                      <tbody>
                        <?php
                        $i = 1; 
                        foreach($sql as $result)
                        {    ?>
                        <tr>
                          <td><?php echo $i;?></td>
                          
                          <td><?php echo $result['CM_Emp_Code']; ?></td>
                          <td><?php echo $result['Emp_Name']; ?></td>
                          <td><?php echo $result['CM_Contact_No'];?></td>
                          <td><a href="student_list.php?c_id=<?php echo $result['Center_Code']; ?>"><?php echo $result['Center_Code']; ?></a></td>
                          <td></td>
                        </tr>

                      <?php $i = $i + 1; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>



            <!-- <div id="displayInfo"></div> -->
        </div>
      </div>
        </div>
      </div>
    </div>




  </div>
</div>

<?php require_once(PATH_CM_INCLUDE.'/footer.php'); ?>


<script>
    $(document).ready(function() {
        $('#cmlisttable').DataTable();   
    });
    
    </script>

<<!-- div>
  <div class="page-title">
    <div class="title_left">
      <h3> Center List</h3>
    </div>
  </div>
  
  <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        <div class="x_title">
          <h2>Search Center</h2>
          
          <div class="clearfix"></div>
        </div>
        <div class="x_content">
        	<form class="form-horizontal" role="form" id="findcmFrm" method="post">
              <div>
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="center_Code">Center Code <span>*</span>:</label>
                  <div class="col-sm-4">
                    <input type="text" class="form-control" id="center_Code" name="center_Code" placeholder="Center Code" />
                  </div>
                </div>
                
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-3">
                    <button type="button" class="btn btn-primary btn-sm" id="search"><i class="fa fa-search"></i> Search</button>
                  </div>
                </div>
                
                <hr />
                
              </div>
            </form>
            <div id="displayInfo"></div>
        </div>
      </div>
    </div>
  </div>
  
  
</div> -->
