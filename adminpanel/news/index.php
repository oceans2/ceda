<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of news
$getNews=$db->ExecuteQuery("SELECT * FROM news ORDER BY News_Id DESC");

?>
<script type="text/javascript" src="news.js" ></script>



<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

<div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">News List</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">News List</li>
          </ol>
          <!-- <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="view-notice.php"><i class="glyphicon glyphicon-share-alt"></i> Add News</a></span> -->
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>
    

<div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="main">
  <div class="page-title">
        <div>
            <div class="col-lg-5 pull-left"><h4><i class="glyphicon glyphicon-plus"></i> Add News</h4></div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div>
  
  
  <div class="clear formbgstyle">
    <form class="form-horizontal" role="form" id="insertNews" method="post">
      <div>
        <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="news">News:<span>*</span>:</label>
          <div class="col-sm-4">
            <textarea  class="form-control input-sm txtarea" id="news" name="news" placeholder="Training Center Address"></textarea>
          </div>
        </div>
        
        
        <hr />
        <div class="form-group">
          <div class="col-sm-3"></div>
          <div class="col-sm-3">
            <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
            <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
          </div>
        </div>
      </div>
    </form>
    <table class="table table-hover table-bordered" id="addedProducts">
      <thead>
        <tr class="success">
          <th>Sno.</th>
          <th>Date &amp; Time ( yy:mm:dd )</th>
          <th>News</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
        <?php 
            $i=1;
            foreach($getNews as $getNewsVal){ ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $getNewsVal['News_Date_Time'];?></td>
          <td><?php echo $getNewsVal['News_Content'];?></td>
          
          <td><button type="button" id="editbtn" class="btn btn-success btn-sm" onClick="window.location.href='edit_news.php?id=<?php echo $getNewsVal['News_Id'];?>'" > <span class="glyphicon glyphicon-edit"></span> Edit </button>
            <button type="button" class="btn btn-danger btn-sm delete" id="<?php echo $getNewsVal['News_Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> Delete </button></td>
        </tr>
        <?php $i++;} ?>
      </tbody>
    </table>
  </div>
</div>
        </div>
      </div>
    </div>


  </div>
</div>


