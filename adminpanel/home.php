<?php
require('../config.php');
include_once(PATH_ADMIN_INCLUDE.'/header.php');
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();
$res=$db->ExecuteQuery("Select DATE_FORMAT(Last_Login_Date,'%d-%m-%Y %H:%i:%s') AS 'Last_Login_Date',Login_Ip from admin_login")
?>

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">

    	<!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Dashboard 1</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Dashboard 1</li>
                    </ol>
                    <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
                </div>
            </div>
        </div>



        <div>
    		<?php echo $res[1]['Last_Login_Date'];?>
			<?php echo $res[1]['Login_Ip'];?>
    	</div>
    	
        <div class="card-group">

        	
	        <!-- card -->
	        <div class="card o-income">
	            <div class="card-body">
	                <div class="d-flex m-b-30 no-block">
	                    <h4 class="card-title m-b-0 align-self-center">Daily Income</h4>
	                    <div class="ml-auto">
	                        <select class="custom-select border-0">
	                            <option selected="">Today</option>
	                            <option value="1">Tomorrow</option>
	                        </select>
	                    </div>
	                </div>
	                <div id="income" style="height:260px; width:100%;"></div>
	                <ul class="list-inline m-t-30 text-center font-12">
	                    <li><i class="fa fa-circle text-success"></i> Growth</li>
	                    <li><i class="fa fa-circle text-info"></i> Net</li>
	                </ul>
	            </div>
	        </div>






	    </div>



    </div>
</div>




