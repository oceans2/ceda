<?php
include('../../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of states
$getState=$db->ExecuteQuery("SELECT * FROM state_master");

// get all list of job rolls
$getDistrict=$db->ExecuteQuery("SELECT dm.District_Id, dm.District_Name, s.State_Name FROM district_master dm

LEFT JOIN state_master s ON dm.State_Id = s.State_Id
ORDER BY State_Name, District_Name ASC
");

?>
<script type="text/javascript" src="district_name.js"></script>



<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Bread crumb and right sidebar toggle -->
    <!-- ============================================================== -->
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Add District</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Add District</li>
          </ol>
          <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button>
        </div>
      </div>
    </div>
    <div>
      <?php echo $res[1]['Last_Login_Date'];?>
      <?php echo $res[1]['Login_Ip'];?>
    </div>
    
    <div class="card-group">
      
      <!-- card -->
      <div class="card o-income">
        <div class="card-body">
          <div class="clear formbgstyle">
            <form class="form-horizontal" role="form" id="insertDistrict" method="post">
              <div>
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="state">State <span>*</span>:</label>
                  <div class="col-sm-3">
                    <select name="state" id="state" class="form-control input-sm" >
                      <option value="">-- Select --</option>
                      <?php foreach($getState as $getStateVal){ ?>
                      <option value="<?php echo $getStateVal['State_Id']; ?>"><?php echo $getStateVal['State_Name']; ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="district_name">Name of District <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="district_name" name="district_name" placeholder="Name of District"  />
                  </div>
                </div>
                <div id="successMsg" class="alert alert-success"></div>
                
                <hr />
                <div class="form-group">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-3">
                    <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
                    <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
                  </div>
                </div>
              </div>
            </form>
            
            
            
            <div class="card">
              <div class="card-body">
                <h4 class="card-title">States & Districts</h4>
                <!-- <h6 class="card-subtitle">Data table example</h6> -->
                <div class="table-responsive m-t-40">
                  <div id="districtList">
                    <table id="addedProduct" class="table table-bordered table-striped">
                      <thead>
                        <tr class="success">
                          <th>Sno.</th>
                          <th>District Name</th>
                          <th>State Name</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $i=1;
                        foreach($getDistrict as $getDistrictVal){ ?>
                        <tr>
                          <td><?php echo $i;?></td>
                          <td><?php echo $getDistrictVal['District_Name'];?></td>
                          <td><?php echo $getDistrictVal['State_Name'];?></td>
                          
                          <td><button type="button" id="editbtn" class="btn btn-success btn-sm" onClick="window.location.href='edit_district.php?id=<?php echo $getDistrictVal['District_Id'];?>'"> <span class="glyphicon glyphicon-edit"></span> Edit </button>
                          <button type="button" class="btn btn-danger btn-sm delete" id="<?php echo $getDistrictVal['District_Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> Delete </button></td>
                        </tr>
                        <?php $i++;} ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script>
    $(document).ready(function() {
      document.getElementById('successMsg').style.display = 'none';
        $('#addedProduct').DataTable();   
    });
    
    </script>
