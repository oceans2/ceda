<?php
include('../../../config.php'); 
include(PATH_ADMIN_INCLUDE.'/header.php');
?>
<script type="text/javascript"  src="course.js" ></script>


<div class="page-wrapper">
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Add Course</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Add Course</li>
          </ol>
          <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;">
            <a class="btn btn-primary" href="list.php" title="Add Course"><i class="glyphicon glyphicon-plus"></i> View Course List</a>
          </span>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>
    <div class="card-group">
      
      <!-- card -->
      <div class="card o-income">
        <div class="card-body">
          <div class="clear formbgstyle">
            <form class="form-horizontal" role="form" id="insertCourse" method="post">
              <div>
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="course_name">Name of Course <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="course_name" name="course_name" placeholder="Name of Course"  />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="application_fee">Application Fees <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="application_fee" name="application_fee" placeholder="Application Fees"  />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="learning_fee">Learning Fees <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="learning_fee" name="learning_fee" placeholder="Learning Fees"  />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="registration_fee">Registration Fees <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="registration_fee" name="registration_fee" placeholder="Registration Fees"  />
                  </div>
                </div>
                
                <div class="form-group">
                  <label class="control-label col-sm-3 mandatory" for="exam_fee">Exam Fees <span>*</span>:</label>
                  <div class="col-sm-3">
                    <input type="text" class="form-control input-sm" id="exam_fee" name="exam_fee" placeholder="Exam Fee"  />
                  </div>
                </div>
                <div id="successcoursecreate" class="alert alert-success"></div>
                <hr />
                
                <div class="form-group">
                  <div class="col-sm-3"></div>
                  <div class="col-sm-3">
                    <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
                    <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
                  </div>
                </div>
              </div>
            </form>
            
          </div>
        </div>
      </div>
    </div>
      
  </div>
</div>

<script>
    $(document).ready(function() {
      document.getElementById('successcoursecreate').style.display = 'none';
    });
    
    </script>