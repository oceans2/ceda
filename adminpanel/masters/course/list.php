<?php
include('../../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of employee 
$getCourse=$db->ExecuteQuery("SELECT Course_Id, Course_Name, Application_Fee, Learning_Fee, Registration_Fee, Exam_Fee
FROM course_master
");


?>
<script type="text/javascript"  src="course.js" ></script>



<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Course List</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Course List</li>
          </ol>
          <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;">
            <a class="btn btn-primary" href="index.php" title="Add Block"><i class="glyphicon glyphicon-plus"></i> Add New Course</a>
          </span>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>
    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          
          
          
          
          <div id="courseList">
            <table id="courselisttable" class="table table-hover table-bordered table-striped">
              <thead>
                <tr class="success">
                  <th>Sno.</th>
                  <th>Course Name</th>
                  <th>Application Fees</th>
                  <th>Learning Fees</th>
                  <th>Registration Fees</th>
                  <th>Exam Fees</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php
                
                if(!empty($getCourse)){
                $i=1;
                foreach($getCourse as $getCourseVal){ ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $getCourseVal['Course_Name'];?></td>
                  <td><?php echo $getCourseVal['Application_Fee'];?></td>
                  <td><?php echo $getCourseVal['Learning_Fee'];?></td>
                  <td><?php echo $getCourseVal['Registration_Fee'];?></td>
                  <td><?php echo $getCourseVal['Exam_Fee'];?></td>
                  
                  <td><a id="editbtn" class="btn btn-success btn-sm" href="edit_course.php?id=<?php echo $getCourseVal['Course_Id'];?>"><span class="glyphicon glyphicon-edit"></span> Edit </a>
                  <button type="button" class="btn btn-danger btn-sm delete" id="<?php echo $getCourseVal['Course_Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> Delete </button></td>
                </tr>
                <?php $i++;}
                }else{
                ?>
                <tr>
                  <td colspan="6" align="center">No Records Found</td>
                </tr>
                <?php } ?>
                
              </tbody>
            </table>
          </div>
        </div>
        
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function() {
        $('#courselisttable').DataTable();   
    });
    
    </script>


