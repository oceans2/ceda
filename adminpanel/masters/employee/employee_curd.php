<?php 
include('../../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
require_once(PATH_LIBRARIES.'/classes/resize.php');
$db = new DBConn();

///*******************************************************
/// Validate that the data already exist or not
///*******************************************************
if($_POST['type']=="validate")
{

	$sql="SELECT Center_Name FROM franchise_master WHERE Center_Name='".$_POST['center_name']."'";
	$res=$db->ExecuteQuery($sql);
		
	if(empty($res))
    {
 		echo 1;
    }
	else
	{
		echo 0;
	}

}


///*******************************************************
/// Get Destination name /////////////////////////////////
///*******************************************************

if($_POST['type']=="getDistrict")
{
	 $sql_district="SELECT District_Id, District_Name FROM district_master WHERE State_Id='".$_POST['stateId']."' ORDER BY District_Name ASC"; 
	 $res=$db->ExecuteQuery($sql_district);
	
	 echo '<option value="">--Select Type--</option>';
	 
	 foreach($res as $val){
		 echo '<option value="'.$val['District_Id'].'">'.$val['District_Name'].'</option>';
	 }
}

///*******************************************************
/// Get Block name ///////////////////////////////////////
///*******************************************************

if($_POST['type']=="getBlocks")
{
	 $sql_district="SELECT Block_Id, Block_Name FROM block_master WHERE District_Id='".$_POST['districtId']."' ORDER BY Block_Name ASC"; 
	 $res=$db->ExecuteQuery($sql_district);
	
	 echo '<option value="">--Select Type--</option>';
	 
	 foreach($res as $val){
		 echo '<option value="'.$val['Block_Id'].'">'.$val['Block_Name'].'</option>';
	 }
}

///*******************************************************
/// To Insert New Branch /////////////////////////////////
///*******************************************************
if($_POST['type']=="addEmployee")
{
	
	/////////////////////////////////
	// Code for student photo
	/////////////////////////////////
	$path = ROOT."/data_images/employee/";
	$path1 = ROOT."/data_images/employee/thumb/";
	
	
	$name = $_FILES['file']['name'];
	$image=explode('.',$name);
	$actual_image_name = time().'.'.$image[1]; // rename the file name
	$tmp = $_FILES['file']['tmp_name'];
	
	
    
	/////////////////////////////////
	// Code for employee  photo
	/////////////////////////////////
	$path_sign = ROOT."/data_images/employee/signature/";
	$path_sign1 = ROOT."/data_images/employee/signature/thumb/";	
	//*******************************
	$name_sign = $_FILES['file_sign']['name'];
	$image_sign=explode('.',$name_sign);
	$actual_image_name_sign = time().'.'.$image[1]; // rename the file name
	$tmp_sign = $_FILES['file_sign']['tmp_name'];

	if(move_uploaded_file($tmp, $path.$actual_image_name) &&  move_uploaded_file($tmp_sign, $path_sign.$actual_image_name_sign))
	{
		
		///////////////////////////////////////////////////////////
		// move the image in the data_images/addmision thumb folder
		///////////////////////////////////////////////////////////
		$resizeObj1 = new resize($path.$actual_image_name);
		$resizeObj1 -> resizeImage(200, 200, 'auto');
		$resizeObj1 -> saveImage($path1.$actual_image_name, 100);

    	///////////////////////////////////////////////////////////
		// move the image in the data_images/addmision thumb folder
		///////////////////////////////////////////////////////////
		$resizeObj1 = new resize($path_sign.$actual_image_name_sign);
		$resizeObj1 -> resizeImage(200, 200, 'auto');
		$resizeObj1 -> saveImage($path_sign1.$actual_image_name_sign, 100);

		//*********************************************************
		$doj=date('Y-m-d',strtotime($_POST['doj']));
		//*********************************************************


		$res=mysql_query("INSERT INTO employee_master (DOJ, EMP_Code, EMP_Name, EMP_Designation, EMP_Image, State_Id, District_Id, Block_Id, EMP_Address, EMP_Contact, EMP_Email, EMP_Salary, Posting_Place, Duty_Time, Visiting_Date_Place,emp_sign,Paymenr_Record,Perfromance) 			
			
		VALUES ('".$doj."', '".$_REQUEST['emp_code']."', '".$_REQUEST['emp_name']."', '".$_REQUEST['designation']."', '".$actual_image_name."', ".$_REQUEST['state'].", ".$_REQUEST['district'].", ".$_REQUEST['block'].", '".$_REQUEST['address']."', '".$_REQUEST['contact_no']."', '".$_REQUEST['email']."', '".$_REQUEST['salary']."', '".$_REQUEST['posting_place']."', '".$_REQUEST['duty_time']."', '".$_REQUEST['Visiting_Date_Place']."','".$actual_image_name_sign."','".$_REQUEST['payment_record']."','".$_REQUEST['perfromance']."')");
		
		
		if(!$res)
		{
		  echo 0;
		}
		 else
		{	
		  echo 1;
		}
		
	}
	else
	echo "failed";
}

///*******************************************************
/// Edit Center
///*******************************************************
if($_POST['type']=="editEmployee")
{
	
	/////////////////////////////////
	// Code for student photo
	/////////////////////////////////
	$path = ROOT."/data_images/employee/";
	$path1 = ROOT."/data_images/employee/thumb/";

	
	$con= mysql_connect(SERVER,DBUSER,DBPASSWORD);
	mysql_query('SET AUTOCOMMIT=0',$con);
	mysql_query('START TRANSACTION',$con);
	
	try
	{
	
		//Upload Here Employee Image	
		if($_REQUEST['imageval']==1)
		{
			$gallary = $_FILES['emp_image']['name'];		
			$tmp2 = $_FILES['emp_image']['tmp_name'];
			$image=explode('.',$gallary);
			$gallary_image = time().'.'.$image[1]; // rename the file name
			
			if(move_uploaded_file($tmp2, $path.$gallary_image))
			  {
				// move the image in the thumb folder
				$resizeObj1 = new resize($path.$gallary_image);
				$resizeObj1 ->resizeImage(50,50,'auto');
				$resizeObj1 -> saveImage($path1.$gallary_image, 100);
				
			  }
			  
			  //Delete Old Image from folder
			  $remove=$db->ExecuteQuery("SELECT EMP_Image FROM employee_master WHERE EMP_Id=".$_REQUEST['emp_id']);
			  if(count($remove)>0 )
			  {
				  if(file_exists($path.$remove[1]['EMP_Image']) && $remove[1]['EMP_Image']!='')
				  {
						unlink($path.$remove[1]['EMP_Image']);
						unlink($path1.$remove[1]['EMP_Image']);
				  }
			  }
		}
		else
		{
			//if Image Is Empty Than
			$gallary_image = $_REQUEST['emp-img'];
		}
	   if($_REQUEST['image_signval']==1)
		{   $path = ROOT."/data_images/employee/signature/";
	        $path1 = ROOT."/data_images/employee/signature/thumb/";
			$gallary_sign = $_FILES['emp_sign_image']['name'];		
			$tmp2 = $_FILES['emp_sign_image']['tmp_name'];
			$image=explode('.',$gallary_sign);
			$emp_signature = time().'.'.$image[1]; // rename the file name
			
			if(move_uploaded_file($tmp2, $path.$emp_signature))
			  {
				// move the image in the thumb folder
				$resizeObj1 = new resize($path.$emp_signature);
				$resizeObj1 ->resizeImage(50,50,'auto');
				$resizeObj1 -> saveImage($path1.$emp_signature, 100);
				
			  }
			  
			  //Delete Old Image from folder
			  $remove=$db->ExecuteQuery("SELECT emp_sign FROM employee_master WHERE EMP_Id=".$_REQUEST['emp_id']);
			  if(count($remove)>0 )
			  {
				  if(file_exists($path.$remove[1]['emp_sign']) && $remove[1]['emp_sign']!='')
				  {
						unlink($path.$remove[1]['emp_sign']);
						unlink($path1.$remove[1]['emp_sign']);
				  }
			  }
		}
		else
		{
			//if Image Is Empty Than
			$emp_signature = $_REQUEST['emp-sign'];
		}

		//*********************************************************
		$doj=date('Y-m-d',strtotime($_POST['doj']));
		//*********************************************************
	
	// Update Employee Master Tabel
	$tblname="employee_master";
	
	$tblfield=array('EMP_Code','EMP_Name','DOJ','EMP_Designation','EMP_Image','State_Id','District_Id','Block_Id','EMP_Address', 'EMP_Contact', 'EMP_Email', 'EMP_Salary', 'Posting_Place', 'Duty_Time', 'Visiting_Date_Place','emp_sign','Paymenr_Record','Perfromance');
	
	$tblvalues=array($_POST['emp_code'], $_POST['emp_name'],$doj ,$_POST['designation'], $gallary_image, $_POST['state'], $_POST['district'], $_POST['block'], $_POST['address'], $_POST['contact_no'], $_POST['email'], $_POST['salary'], $_POST['posting_place'], $_POST['duty_time'], $_POST['Visiting_Date_Place'],$emp_signature,$_POST['payment_record'],$_POST['perfromance']);
	
	$condition="EMP_Id=".$_POST['emp_id'];
	$res=$db->updateValue($tblname,$tblfield,$tblvalues,$condition);
	
	if (empty($res)) 
		{
				throw new Exception('0');
		}
		
	
		mysql_query("COMMIT",$con);
		echo 1;
	}
	catch(Exception $e)
	{
		echo  $e->getMessage();
		mysql_query('ROLLBACK',$con);
		mysql_query('SET AUTOCOMMIT=1',$con);
	}
}


///*******************************************************
/// Delete row from franchise_master table
///*******************************************************
if($_POST['type']=="delete")
{
	/////////////////////////////////
	// Code for student photo
	/////////////////////////////////
	$path = ROOT."/data_images/employee/";
	$path1 = ROOT."/data_images/employee/thumb/";
	
	 $tblname="employee_master";
	 $condition="EMP_Id=".$_POST['emp_id'];
	 $res=$db->deleteRecords($tblname,$condition);
	 
	 unlink($path.$remove[1]['EMP_Image']);
	 unlink($path1.$remove[1]['EMP_Image']);
}

if($_POST['type']=="EMPCodeCheck")
{  $emp_code=$_REQUEST['emp_code'];
	$sql="SELECT EMP_Code FROM employee_master WHERE EMP_Code='$emp_code' AND EMP_Id<>".$_REQUEST['emp_id'];
	$res=$db->ExecuteQuery($sql);
  if(!empty($res))
  {
  	echo 1;
  }
  else
  {
  	echo 0;
  }
   
}
if($_POST['type']=="allsearch")
{
            $cndition="";
				if(isset($_REQUEST['emp_code_test']))
				{
					if($_REQUEST['emp_code_test']!="")
					{
						$cndition.="AND  e.EMP_Code='".$_REQUEST['emp_code_test']."'";
					}
				}


	// get all list of employee 
$getemployee=$db->ExecuteQuery("SELECT DATE_FORMAT(DOJ,'%d-%m-%Y') AS DOJ, e.EMP_Id, e.EMP_Code, e.EMP_Image, e.EMP_Name, e.EMP_Designation, b.Block_Name, d.District_Name, s.State_Name, e.EMP_Contact, EMP_Email,Status,EMP_Salary

FROM employee_master e

LEFT JOIN block_master b ON e.Block_Id = b.Block_Id
LEFT JOIN district_master d ON e.District_Id = d.District_Id
LEFT JOIN state_master s ON d.State_Id = s.State_Id WHERE 1=1 $cndition

");

?>
 <?php 
			  if(!empty($getemployee))
			 {
			            $i=1;
			            foreach($getemployee as $getemployeeVal){ ?>
			        <tr>
			          <td><?php echo $i;?></td>
			          <td><?php echo $getemployeeVal['DOJ'];?></td>
			          <td><img width="50px;" src="<?php echo PATH_DATA_IMAGE ?>/employee/thumb/<?php echo $getemployeeVal['EMP_Image'];?>" alt="" /></td>
			          <td><?php echo $getemployeeVal['EMP_Code'];?></td>
			          <td><?php echo $getemployeeVal['EMP_Name'];?></td>
			          <td><?php echo $getemployeeVal['EMP_Designation'];?></td>
			          <td><?php echo $getemployeeVal['EMP_Salary'];?></td>
			          <td><?php echo $getemployeeVal['Block_Name'];?></td>
			          <td><?php echo $getemployeeVal['District_Name'];?></td>
			          <td><?php echo $getemployeeVal['State_Name'];?></td>
			          <td><?php echo $getemployeeVal['EMP_Contact'];?></td>
			          <td><?php echo $getemployeeVal['EMP_Email'];?></td>
			          
			          <td><button type="button" id="editbtn" class="btn btn-success btn-sm" onClick="window.location.href='edit_employee.php?id=<?php echo $getemployeeVal['EMP_Id'];?>'" > <span class="glyphicon glyphicon-edit"></span> Edit </button>
			            <button type="button" class="btn btn-danger btn-sm delete" id="<?php echo $getemployeeVal['EMP_Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> Delete </button>
			            <button type="button" class="btn btn-danger btn-sm block" id="block-<?php echo $getemployeeVal['EMP_Id']; ?>" name="block"> <span class="glyphicon"></span> 
			             <?php if($getemployeeVal['Status']==0){echo "BLOCK";
			             } 
			             else
                         {
	                         echo "UNBLOCK";
                         }
                         ?> </button>
                        
			            </td>
			        </tr>
			        <?php $i++;} ?>
			<?php
			}
			else
			{?>
         <tr><td colspan="13" align="center">NO RECORD FOUND </td></tr>
				
			<?php 
		}
}

if($_POST['type']=="block")
{ 
 $id=explode('-',$_POST['emp_id']);
 $tblfield=array('Status');
 $sql="SELECT Status FROM employee_master WHERE 	EMP_Id='".$id[1]."'";
$getemployee=$db->ExecuteQuery($sql);

if($getemployee[1]['Status']==1)
{
	$tblvalues=array('0');
}
else
{
	$tblvalues=array('1');
}
	$condition="EMP_Id=".$id[1];
	$res=$db->updateValue('employee_master',$tblfield,$tblvalues,$condition);
	if($res)
	{
		echo 1;
	}
}
?>