<?php 
require('../config.php');
include(PATH_ADMIN_INCLUDE.'/head.php');
?>

<script type="text/javascript">
	$(document).ready(function(){
		$("#msg").hide();
		document.onkeydown = function(event) {
		   if (event.keyCode == 13) {
			   $("#login").trigger("click");
		   }
		}
		$("#login").click(function(){
			$("#msg").hide();
			$("#msg").text('');
			var user_name =$("#user").val();
			var password =$("#password").val();
			if (user_name=="")
			{
				$("#msg").append("<strong>Warning!</strong> Enter User Name");
				$("#msg").show();
				return false;
			}
			if (password=="")
			{
				$("#msg").append("<strong>Warning!</strong> Enter Password");
				$("#msg").show();
				return false;
			}
			//alert(password);
			var x;
			$.ajax(
			{
				url:'check_login.php',
				type:'POST',
				data:{user:user_name,password:password},
				async:false,
				success:function(data){ //alert(data);
					x=data;
					
					if(x=="true")
					{
						 //alert(x);
						//exit();
						//window.location
						//window.location.href="home.php";
						window.location.href= "home.php";
						//alert("ABC");
					}
					else
					{
						$("#msg").append("<strong>Warning!</strong> Incorrect Username/Password!");
						$("#msg").show();
					}
					
				}
			});// eof ajax
	
			
		}); // eof login
	});// eof ready function
</script>







<body>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <!-- <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Elite admin</p>
        </div>
    </div> -->    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <section id="wrapper" class="login-register login-sidebar" style="background-image:url(assets/images/background/login-register.jpg);">
        <div class="login-box card">
            <div class="card-body">
                <form class="form-horizontal form-material" id="index" method="post">
                    <!-- <a href="javascript:void(0)" class="text-center db"><img src="assets/images/logo-icon.png" alt="Home" /><br/><img src="assets/images/logo-text.png" alt="Home" /></a> -->
                    <h2>Administrator Login</h2>
                    <div class="form-group m-t-40">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Username" id="user">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12">
                            <input class="form-control" type="password" required="" placeholder="Password" id="password">
                        </div>
                    </div>
                    <!-- <div class="form-group row">
                        <div class="col-md-12">
                            <div class="custom-control custom-checkbox">
                                <input type="checkbox" class="custom-control-input" id="customCheck1">
                                <label class="custom-control-label" for="customCheck1">Remember me</label>
                                <a href="javascript:void(0)" id="to-recover" class="text-dark pull-right"><i class="fa fa-lock m-r-5"></i> Forgot pwd?</a> 
                            </div>     
                        </div>
                    </div> -->
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-info btn-lg btn-block text-uppercase btn-rounded" type="button" id="login">Log In</button>
                        </div>
                    </div>
                    <!-- <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12 m-t-10 text-center">
                            <div class="social"><a href="javascript:void(0)" class="btn  btn-facebook" data-toggle="tooltip" title="Login with Facebook"> <i aria-hidden="true" class="fa fa-facebook"></i> </a> <a href="javascript:void(0)" class="btn btn-googleplus" data-toggle="tooltip" title="Login with Google"> <i aria-hidden="true" class="fa fa-google-plus"></i> </a> </div>
                        </div>
                    </div> -->
                    <!-- <div class="form-group m-b-0">
                        <div class="col-sm-12 text-center">
                            Don't have an account? <a href="pages-register2.html" class="text-primary m-l-5"><b>Sign Up</b></a>
                        </div>
                    </div> -->
                </form>
                <!-- <form class="form-horizontal" id="recoverform" action="index.html">
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <h3>Recover Password</h3>
                            <p class="text-muted">Enter your Email and instructions will be sent to you! </p>
                        </div>
                    </div>
                    <div class="form-group ">
                        <div class="col-xs-12">
                            <input class="form-control" type="text" required="" placeholder="Email">
                        </div>
                    </div>
                    <div class="form-group text-center m-t-20">
                        <div class="col-xs-12">
                            <button class="btn btn-primary btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">Reset</button>
                        </div>
                    </div>
                </form> -->
                <div class="alert alert-danger alert-dismissable" id="msg"></div>
            </div>
        </div>
    </section>
    <!-- ============================================================== -->
    <!-- End Wrapper -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- All Jquery -->
    <!-- ============================================================== -->
    
    <!--Custom JavaScript -->
    
    
</body>

</html>