<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of Notice /
$getnotice=$db->ExecuteQuery("SELECT Notice_Id, DATE_FORMAT(Notice_Date,'%d-%m-%Y') AS Notice_Date, Notice, Student_Name, Student_Code

FROM ho_notice hn

LEFT JOIN student_master st ON hn.Student_Id = st.Student_Id");

?>
<script type="text/javascript" src="notice.js"></script>

<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

<div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">List of Notice</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">List of Notice</li>
          </ol>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
          <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="index.php"><i class="glyphicon glyphicon-share-alt"></i> Send Notice</a></span>
        </div>
      </div>
    </div>


    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="x_content">
      <table id="noticelist" class="table table-hover table-bordered table-condensed">
              <thead>
                <tr class="success">
                  <th>Sno.</th>
                  <th width="90">Notice Date</th>
                  <th>Student Name- Code</th>
                  <th>Notice</th>                  
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                <?php 
                    $i=1;
                    foreach($getnotice as $getnoticeVal){ ?>
                <tr>
                  <td><?php echo $i;?></td>
                  <td><?php echo $getnoticeVal['Notice_Date']; ?></td>
                  <td><?php echo $getnoticeVal['Student_Name'].'-'.$getnoticeVal['Student_Code'];?></td>
                  <td><?php echo $getnoticeVal['Notice']; ?></td>
                  
                  <td><button type="button" id="editbtn" class="btn btn-success btn-xs" onClick="window.location.href='edit_notice.php?id=<?php echo $getnoticeVal['Notice_Id'];?>'" > <span class="glyphicon glyphicon-edit"></span> </button>
                    <button type="button" class="btn btn-danger btn-xs delete" id="<?php echo $getnoticeVal['Notice_Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> </button></td>
                </tr>
                <?php $i++;} ?>
              </tbody>
            </table>
    </div>
        </div>
      </div>
    </div>

  </div>
</div>
<script>
$(document).ready(function() {
      
        $('#noticelist').DataTable();  
    });

    </script>


<?php require_once(PATH_ADMIN_INCLUDE.'/footer.php'); ?>



