<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of employee 
$getBlock=$db->ExecuteQuery("SELECT * FROM exam WHERE Action!='1'");
?>
<script>
$(document).on("click",".compleate", function(){
	
	        var id=$(this).attr('id');
			var formdata = new FormData();
			formdata.append('type', "Examcompleate");
			formdata.append('id',id);
			
				$.ajax({
					type:"POST",
					url :"curd.php",
					data:formdata,
					success: function(data){ //alert(data);
					      
							if(data==0)
							{
							window.location.replace("view.php");
							}
					},
					cache: false,
					contentType: false,
					processData: false
				});//eof ajax
		
	});

</script>


<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Exam List</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Exam List</li>
          </ol>
          <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="index.php"><i class="glyphicon glyphicon-share-alt"></i> Create New Exam</a></span>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>


<div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="main">
  
    
    <!-- <div style="background:#FCF1B1; padding:10px;"> -->
      <form class="form-horizontal" role="form" id="searchFrm" method="post">
          
            
          
            <div><button type="button" class="btn btn-primary btn-sm" id="search">Search</button></div>
        </form>
    <!-- </div> -->
    <div id="blockList">
      <table class="table table-hover table-bordered">
          <thead>
            <tr class="success">
              <th>Sno.</th>
              <th>Exam Name</th>
              <th>Center Code</th>
              <th>Center Name</th>
              <th>Date</th>
              <th>Time</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <?php 
            
            if(!empty($getBlock)){
                $i=1;
                foreach($getBlock as $getBlockVal){ ?>
            <tr>
              <td><?php echo $i;?></td>
              <td><?php echo $getBlockVal['Examname'];?></td>
              <td><?php echo $getBlockVal['Centercode'];?></td>
              <td><?php echo $getBlockVal['Centername'];?></td>
              <td><?php echo $getBlockVal['Date'];?></td>
              <td><?php echo $getBlockVal['Time'];?></td>
        <td><button type="button" id="<?php echo $getBlockVal['Id']; ?>" class="btn btn-success btn-sm compleate" > <span class="glyphicon glyphicon-edit"></span> Completed </button></td>
            
            </tr>
            <?php $i++;} 
            }else{
            ?>
            <tr>
                <td colspan="5">No Records Found</td>
            </tr>
            <?php } ?>
            
          </tbody>
        </table>
    </div>
</div>
        </div>
      </div>
    </div>



  </div>
</div>



