// JavaScript Document
$(document).ready(function(){
	
	///////////////////////////////////
	// Add Block Form Validation
	////////////////////////////////////
	$("#insertBlock").validate({
		rules: 
		{ 
			state: 
			{ 
				required: true,
			},
			district: 
			{ 
				required: true,
			},
			blockName:
			{
				required: true,
				BlockExist:true
			}
		},
		messages:
		{
			
		}
	});// eof validation
	
	
	///////////////////////////////////
	// Edit Jobroll form validation
	////////////////////////////////////
	$("#editBlock").validate({
		rules: 
		{ 
			state: 
			{ 
				required: true,
			},
			district: 
			{ 
				required: true,
			},
			blockName:
			{
				required: true,
				BlockEditExist:true
			}
		},
		messages:
		{
			
		}
	});// eof validation
	
	
	///////////////////////////////////
	// Edit Jobroll form validation
	////////////////////////////////////
	$("#searchFrm").validate({
		rules: 
		{ 
			state: 
			{ 
				required: true,
			},
			district: 
			{ 
				required: true,
			}
		},
		messages:
		{
			
		}
	});// eof validation
	
	
	
	
	//////////////////////////////////
	// on click of submit button
	//////////////////////////////////
	$(document).on("click","#submit", function(){
		
		flag=$("#insertBlock").valid();
		
		if (flag==true)
		{			
			var formdata = new FormData();
			formdata.append('type', "addBlock");
			formdata.append('blockName', $("#blockName").val());
			formdata.append('district', $("#district").val());
	
			var x;
			$.ajax({
			   type: "POST",
			   url: "block_curd.php",
			   data:formdata,
			   success: function(data){ //alert(data);
				  x=data;
				   
				  if(x==1)
				  {
					window.location.replace("index.php");
				  }
			   },
			   cache: false,
			   contentType: false,
			   processData: false
			});//eof ajax
		}// eof if condition
		
	});
	
	
	
});//eof of ready function