<?php 
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();

///*******************************************************
/// Validate that the data already exist or not
///*******************************************************


///*******************************************************
/// To Insert New District ///////////////////////////////
///*******************************************************
if($_POST['type']=="Examcreate")
{
	$con= mysql_connect(SERVER,DBUSER,DBPASSWORD);
	mysql_query('SET AUTOCOMMIT=0',$con);
	mysql_query('START TRANSACTION',$con);
	
	try
	{	
	   $date=date("Y-m-d",strtotime($_POST['doe']));
		$tablename = "exam";		
		$tblfield=array('Examname','Date','Time','Centercode','Centername');		
		$tblvalues=array($_POST['exam'],$date,$_POST['usr_time'],$_POST['centercode'],$_POST['centername']);
		
		$res=$db->valInsert($tablename,$tblfield,$tblvalues);
		
		if(!$res)
		{
			throw new Exception('0');
		}
		
		mysql_query("COMMIT",$con);
		echo 1;
	}
	catch(Exception $e)
	{
		echo  $e->getMessage();
		mysql_query('ROLLBACK',$con);
		mysql_query('SET AUTOCOMMIT=1',$con);
	}
}

///*******************************************************
/// Edit Center
///*******************************************************
if($_POST['type']=="Examcompleate")
{
	$tblname="exam";	
	$tblfield=array('Action');
	$tblvalues=array('1');
	$condition="Id=".$_POST['id'];
	$res=$db->updateValue($tblname,$tblfield,$tblvalues,$condition);
	
	if (empty($res))
	{
		echo 1;
	}
	else
	{
		echo 0;
	}
}


///*******************************************************
/// Search Districts state wise
///*******************************************************
if($_POST['type']=="searchByDistrict")
{
	
	$res=$db->ExecuteQuery("SELECT b.Block_Id, b.Block_Name, dm.District_Name, s.State_Name 
	FROM block_master b
	
	LEFT JOIN district_master dm ON b.District_Id = dm.District_Id
	LEFT JOIN state_master s ON dm.State_Id = s.State_Id
	WHERE dm.District_Id=".$_POST['district']);
	
	
	echo "<table class='table table-hover table-bordered'>
          <thead>
            <tr class='success'>
              <th>Sno.</th>
              <th>Block Name</th>
              <th>District</th>
              <th>State</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>";
	$i=1;
	foreach($res as $val){
		echo "<tr><td>".$i."</td><td>".$val['Block_Name']."</td><td>".$val['District_Name']."</td><td>".$val['State_Name']."</td>
		
		<td><a id='editbtn' class='btn btn-success btn-sm' href='edit_block.php?id='".$val['Block_Id']."'><span class='glyphicon glyphicon-edit'></span> Edit </a>
                <button type='button' class='btn btn-danger btn-sm delete' id='".$val['Block_Id']."' name='delete'> <span class='glyphicon glyphicon-trash'></span> Delete </button></td>
		
		</tr>";
			
		$i++;
		
	}
	
	echo "</tbody></table>";
}


///*******************************************************
/// Delete row from franchise_master table
///*******************************************************
if($_POST['type']=="delete")
{
	 $tblname="block_master";
	 $condition="Block_Id=".$_POST['block_id'];
	 $res=$db->deleteRecords($tblname,$condition);
}


?>