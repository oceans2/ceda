<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
?>
<script>
// JavaScript Document
$(document).ready(function(){
	
	///////////////////////////////////
	// Add Block Form Validation
	////////////////////////////////////
	$("#insertBlock").validate({
		rules: 
		{ 
			exam: 
			{ 
				required: true,
			},
			doe: 
			{ 
				required: true,
			},
			usr_time:
			{
				required: true,
				
			},
			centername:
			{
				required: true,
			},
			centercode:
			{
				required: true,
			},
		},
		messages:
		{
			
		}
	});// eof validation
	$(document).on("click","#submit", function(){
		
		flag=$("#insertBlock").valid();
		
		if (flag==true)
		{	
		    
			var formdata = new FormData();
			formdata.append('type', "Examcreate");
			formdata.append('exam', $("#exam").val());
			formdata.append('doe', $("#doe").val());
			formdata.append('usr_time', $("#usr_time").val());
			formdata.append('centercode', $("#centercode").val());
			formdata.append('centername', $("#centername").val());
				$.ajax({
					type:"POST",
					url :"curd.php",
					data:formdata,
					success: function(data){ alert(data);
					      
							if(data==1)
							{
							window.location.replace("view.php");
							}
					},
					cache: false,
					contentType: false,
					processData: false
				});//eof ajax
				
		}
	});
});
</script>

<div class="page-wrapper">
	<!-- ============================================================== -->
	<!-- Container fluid  -->
	<!-- ============================================================== -->
	<div class="container-fluid">

<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h4 class="text-themecolor">Add Exam</h4>
			</div>
			<div class="col-md-7 align-self-center text-right">
				<div class="d-flex justify-content-end align-items-center">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
						<li class="breadcrumb-item active">Add Exam</li>
					</ol>
					<span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="view.php"><i class="glyphicon glyphicon-share-alt"></i> View  List</a></span>
					<!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
				</div>
			</div>
		</div>


		<div class="card-group">
			<div class="card o-income">
				<div class="card-body">
					<div class="main">
  
  
  
  <div class="clear formbgstyle">
    <form class="form-horizontal" role="form" id="insertBlock" method="post">
      <div>
        <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="state">Exam Name <span>*</span>:</label>
          <div class="col-sm-3">
           <input type="text" name="exam" id="exam" value=""class="form-control input-sm" />
          </div>
        </div>
         <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="state">Center Code<span>*</span>:</label>
          <div class="col-sm-3">
           <input type="text" name="centercode" id="centercode" value=""class="form-control input-sm" />
          </div>
        </div>
         <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="state">Center Name <span>*</span>:</label>
          <div class="col-sm-3">
           <input type="text" name="centername" id="centername" value=""class="form-control input-sm" />
          </div>
        </div>
        
        <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="district">Date <span>*</span>:</label>
          <div class="col-sm-3" id="districtDiv">
           <input type="text" id="doe" name="doe" class="form-control datetimepicker" placeholder="DD/MM/YYYY">
          </div>
        </div>
        
        <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="blockName">Time<span>*</span>:</label>
          <div class="col-sm-3">
            <input type="time" name="usr_time" id="usr_time"class="form-control"/>
          </div>
        </div>
        
        
        <hr />
        <div class="form-group">
          <div class="col-sm-3"></div>
          <div class="col-sm-3">
            <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
            <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
				</div>
			</div>
		</div>


		

	</div>
</div>




<?PHp include(PATH_ADMIN_INCLUDE.'/footer.php'); ?>
