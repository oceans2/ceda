<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of rm logins 
$getimage=$db->ExecuteQuery("SELECT S_NO,Image_Name FROM image_gallery");


?>
<style>
  body {
    padding: 30px 0px;
}

#lightbox .modal-content {
    display: inline-block;
    text-align: center;   
}

#lightbox .close {
    opacity: 1;
    color: rgb(255, 255, 255);
    background-color: rgb(25, 25, 25);
    padding: 5px 8px;
    border-radius: 30px;
    border: 2px solid rgb(255, 255, 255);
    position: absolute;
    top: -15px;
    right: -55px;
    
    z-index:1032;
}
  </style>
  <script>
$(document).ready(function() {
    var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width() - 100,
                'maxHeight': $(window).height() - 100
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});
  </script>

<script type="text/javascript"  src="image.js" ></script>
<div id="loading">
    <div class="loader-block"><i class="fa-li fa fa-spinner fa-spin spinloader"></i></div>
</div>

<div class="page-wrapper">
    <!-- ============================================================== -->
    <!-- Container fluid  -->
    <!-- ============================================================== -->
    <div class="container-fluid">


<div class="row page-titles">
            <div class="col-md-5 align-self-center">
                <h4 class="text-themecolor">Image List</h4>
            </div>
            <div class="col-md-7 align-self-center text-right">
                <div class="d-flex justify-content-end align-items-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                        <li class="breadcrumb-item active">Image List</li>
                    </ol>
                    <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
                     <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="add_image.php"><i class="glyphicon glyphicon-share-alt"></i>New Image Upload</a></span> 
                </div>
            </div>
        </div>


<div class="card-group">
            <div class="card o-income">
                <div class="card-body">
                    <div class="main">
   <!--  <div class="page-title">
        <div>
            <div class="col-lg-5 pull-left"><h4><i class="glyphicon glyphicon-list"></i>  List</h4></div>
            <div class="col-lg-5 pull-right">
                <div class="ef_header_tools pull-right">
                    <a class="btn btn-primary" href="add_image.php" title="Add course"><i class="glyphicon glyphicon-plus"></i>&nbsp; New Image Upload</a>
                </div>
            </div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div> -->
  
    <table class="table table-hover table-bordered" id="addedProducts">
      <thead>
        <tr class="success">
          <th><input type="checkbox" name="checkbox"  id="checkbox_all_delete" value=""></th>
          <th>Sno.</th>
          <th>Image</th>
        </tr>
      </thead>
      <tbody>
        <?php 
            $i=1;
            foreach($getimage as $getrmloginVal){ ?>
        <tr>
         <td><input type="checkbox" name="checkbox2" id="<?php echo$getrmloginVal['S_NO']; ?>"  class="checkbox_delete"></td>
          <td><?php echo $i;?></td>

           <td><a href="#" class="thumbnail" data-toggle="modal" data-target="#lightbox"> 
            <img width="10%" src="<?php echo PATH_ROOT ?>/image_gallery/thumb/<?php echo $getrmloginVal['Image_Name'];?>" alt="" />
        </a></td>          
         
        </tr>
        <?php $i++;} ?>
      </tbody>
    </table>
</div>
                </div>
            </div>
        </div>



    </div>
</div>



<!-- Model Content Here -->
<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
        <div class="modal-content">
            <div class="modal-body">
                <img src="" alt="" />
            </div>
        </div>
    </div>
</div>