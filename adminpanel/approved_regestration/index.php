<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();


?>
<script type="text/javascript">

  // JavaScript Document
$(document).ready(function(){
  //////////////////////////////////
  // All Search
  //////////////////////////////////    
   $.ajax({
        url:"curd.php",
        type: "POST",
        data: {type:"allsearch"},
        async:false,
        success: function(data){  //alert(data);
          if(data)
          {
             $('#addedProducts tbody').html(data);
          }
        }
      });

});
</script>

<script type="text/javascript" src="list.js" ></script>

<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <!-- <h4 class="text-themecolor">Add District</h4> -->
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">List of Received Registration Fees</li>
          </ol>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="main">
  <div class="page-title">
        <div>
            <div class="col-sm-5 pull-left">
              <h4><i class="glyphicon glyphicon-plus"></i>List of Received Registration Fees</h4>
            </div>
        
            <div class="col-sm-5 pull-left">
              <form class="form-horizontal" role="form" id="search_student" method="post">
                    <div>
                      <div class="col-sm-5">Center Code : <input type="text" name="center_code" value="" class="form-control input-sm center_code" id="center_code"></div>
                      <div class="col-sm-5">Date : <input type="text" id="regDate" name="regDate" class="form-control input-sm datetimepicker" placeholder="dd-mm-yyyy" value=""></div>
                      <div class="col-sm-2"><input type="button" class="btn btn-primary btn-sm" id="submit_search" value="Search"></div>
                      <div class="clearfix">&nbsp;</div>
                    </div>
              </form>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
  
  
        <div class="clear formbgstyle" style="margin-top:10px;">
          
          <table class="table table-hover table-bordered" id="addedProducts">
            <thead>
              <tr class="success">
                <th>Sno.</th>
                 <th>Date Of Payment</th>
                 <th>Amount</th>
                 <th>Mode Of Payment</th>
                 <th>Cheque No/DD No</th>
                 <th>Transaction Number</th>
                 <th>Center Code</th>
                 <th>Action</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>

        </div>
  </div>
</div>
        </div>
      </div>
    </div>

    


  </div>
</div>



<div class="main">
  <div class="page-title">
        <div>
            <div class="col-sm-5 pull-left">
              <h4><i class="glyphicon glyphicon-plus"></i>List of Received Registration Fees</h4>
            </div>
        
            <div class="col-sm-5 pull-left">
              <form class="form-horizontal" role="form" id="search_student" method="post">
                    <div>
                      <div class="col-sm-5">Center Code : <input type="text" name="center_code" value="" class="form-control input-sm center_code" id="center_code"></div>
                      <div class="col-sm-5">Date : <input type="text" id="regDate" name="regDate" class="form-control input-sm datetimepicker" placeholder="dd-mm-yyyy" value=""></div>
                      <div class="col-sm-2"><input type="button" class="btn btn-primary btn-sm" id="submit_search" value="Search"></div>
                      <div class="clearfix">&nbsp;</div>
                    </div>
              </form>
            </div>
            <div class="clearfix">&nbsp;</div>
        </div>
  
  
        <div class="clear formbgstyle" style="margin-top:10px;">
          
          <table class="table table-hover table-bordered" id="addedProducts">
            <thead>
              <tr class="success">
                <th>Sno.</th>
                 <th>Date Of Payment</th>
                 <th>Amount</th>
                 <th>Mode Of Payment</th>
                 <th>Cheque No/DD No</th>
                 <th>Transaction Number</th>
                 <th>Center Code</th>
                 <th>Action</th>
              </tr>
            </thead>
            <tbody>
              
            </tbody>
          </table>

        </div>
  </div>
</div>
<?php include(PATH_ADMIN_INCLUDE.'/footer.php'); ?>