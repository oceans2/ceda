<?php 
include(PATH_MASTERS.'/include/head.php');
if($_SESSION['Login_Id']=="")
{?>
<script>
		window.location.href = '<?php echo PATH_ADMIN_LINK.'/index.php'; ?>';
	</script>
<?php } ?>

<script>
	$(document).ready(function() {
		$("#logoff").click(function(){
            event.preventDefault();
			$.ajax(
			{
				url:'<?php echo PATH_ADMIN_LINK.'/logout.php'; ?>',
				type:'POST',
				data:{},
				async:false,
				success:function(data){
				if (data=="true")
					{
                        //echo PATH_ADMIN_LINK.'/index.php';
                        //echo ROOT
						document.location.href='<?php echo PATH_ADMIN_LINK.'/index.php'; ?>';
					}
				}
			});//eof ajax
		});// eof click function
	});// eof ready function
</script>



<body class="skin-default-dark fixed-layout">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">


        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <nav class="navbar top-navbar navbar-expand-md navbar-dark">
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <div class="navbar-header">
                    <a class="navbar-brand" href="index.html">
                        <!-- Logo icon --><b>
                            <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                            <!-- Dark Logo icon -->
                            <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/logo-icon.png" alt="homepage" class="dark-logo" />
                            <!-- Light Logo icon -->
                            <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/logo-light-icon.png" alt="homepage" class="light-logo" />
                        </b>
                        <!--End Logo icon -->
                        <!-- Logo text --><span>
                         <!-- dark Logo text -->
                         <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/logo-text.png" alt="homepage" class="dark-logo" />
                         <!-- Light Logo text -->    
                         <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
                </div>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <div class="navbar-collapse">
                    <!-- ============================================================== -->
                    <!-- toggle and nav items -->
                    <!-- ============================================================== -->
                    <ul class="navbar-nav mr-auto">
                        <!-- This is  -->
                        <li class="nav-item hidden-sm-up"> <a class="nav-link nav-toggler waves-effect waves-light" href="javascript:void(0)"><i class="ti-menu"></i></a></li>
                        <!-- ============================================================== -->
                        <!-- Comment -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="ti-email"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox animated bounceInDown">
                                <span class="with-arrow"><span class="bg-primary"></span></span>
                                <ul>
                                    <li>
                                        <div class="drop-title bg-primary text-white">
                                            <h4 class="m-b-0 m-t-5">4 New</h4>
                                            <span class="font-light">Notifications</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-danger btn-circle"><i class="fa fa-link"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Luanch Admin</h5> <span class="mail-desc">Just see the my new admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-success btn-circle"><i class="ti-calendar"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Event today</h5> <span class="mail-desc">Just a reminder that you have event</span> <span class="time">9:10 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-info btn-circle"><i class="ti-settings"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Settings</h5> <span class="mail-desc">You can customize this template as you want</span> <span class="time">9:08 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="btn btn-primary btn-circle"><i class="ti-user"></i></div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center m-b-5" href="javascript:void(0);"> <strong>Check all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Comment -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Messages -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="2" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="icon-note"></i>
                                <div class="notify"> <span class="heartbit"></span> <span class="point"></span> </div>
                            </a>
                            <div class="dropdown-menu mailbox animated bounceInDown" aria-labelledby="2">
                                <span class="with-arrow"><span class="bg-danger"></span></span>
                                <ul>
                                    <li>
                                        <div class="drop-title text-white bg-danger">
                                            <h4 class="m-b-0 m-t-5">5 New</h4>
                                            <span class="font-light">Messages</span>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="message-center">
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/users/1.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/users/2.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/users/3.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                            </a>
                                            <!-- Message -->
                                            <a href="javascript:void(0)">
                                                <div class="user-img"> <img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/users/4.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                                <div class="mail-contnet">
                                                    <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                            </a>
                                        </div>
                                    </li>
                                    <li>
                                        <a class="nav-link text-center link m-b-5" href="javascript:void(0);"> <b>See all e-Mails</b> <i class="fa fa-angle-right"></i> </a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End Messages -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- Search -->
                        <!-- ============================================================== -->
                        <li class="nav-item search-box"> <a class="nav-link waves-effect waves-dark" href="javascript:void(0)"><i class="ti-search"></i></a>
                            <form class="app-search">
                                <input type="text" class="form-control" placeholder="Search &amp; enter"> <a class="srh-btn"><i class="ti-close"></i></a>
                            </form>
                        </li>
                    </ul>
                    <ul class="navbar-nav my-lg-0">
                        <!-- ============================================================== -->
                        <!-- mega menu -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown mega-dropdown"> <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="ti-layout-width-default"></i></a>
                            <div class="dropdown-menu animated bounceInDown">
                                <ul class="mega-dropdown-menu row">
                                    <li class="col-lg-3 col-xlg-2 m-b-30">
                                        <h4 class="m-b-20">CAROUSEL</h4>
                                        <!-- CAROUSEL -->
                                        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                                            <div class="carousel-inner" role="listbox">
                                                <div class="carousel-item active">
                                                    <div class="container"> <img class="d-block img-fluid" src="<?php echo PATH_IMAGE_NEW ?>/assets/images/big/img1.jpg" alt="First slide"></div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container"><img class="d-block img-fluid" src="<?php echo PATH_IMAGE_NEW ?>/assets/images/big/img2.jpg" alt="Second slide"></div>
                                                </div>
                                                <div class="carousel-item">
                                                    <div class="container"><img class="d-block img-fluid" src="<?php echo PATH_IMAGE_NEW ?>/assets/images/big/img3.jpg" alt="Third slide"></div>
                                                </div>
                                            </div>
                                            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev"> <span class="carousel-control-prev-icon" aria-hidden="true"></span> <span class="sr-only">Previous</span> </a>
                                            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next"> <span class="carousel-control-next-icon" aria-hidden="true"></span> <span class="sr-only">Next</span> </a>
                                        </div>
                                        <!-- End CAROUSEL -->
                                    </li>
                                    <li class="col-lg-3 m-b-30">
                                        <h4 class="m-b-20">ACCORDION</h4>
                                        <!-- Accordian -->
                                        <div id="accordion" class="nav-accordion" role="tablist" aria-multiselectable="true">
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingOne">
                                                    <h5 class="mb-0">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                  Collapsible Group Item #1
                                                </a>
                                              </h5> </div>
                                                <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high. </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingTwo">
                                                    <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                  Collapsible Group Item #2
                                                </a>
                                              </h5> </div>
                                                <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                </div>
                                            </div>
                                            <div class="card">
                                                <div class="card-header" role="tab" id="headingThree">
                                                    <h5 class="mb-0">
                                                <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                  Collapsible Group Item #3
                                                </a>
                                              </h5> </div>
                                                <div id="collapseThree" class="collapse" role="tabpanel" aria-labelledby="headingThree">
                                                    <div class="card-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="col-lg-3  m-b-30">
                                        <h4 class="m-b-20">CONTACT US</h4>
                                        <!-- Contact -->
                                        <form>
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputname1" placeholder="Enter Name"> </div>
                                            <div class="form-group">
                                                <input type="email" class="form-control" placeholder="Enter email"> </div>
                                            <div class="form-group">
                                                <textarea class="form-control" id="exampleTextarea" rows="3" placeholder="Message"></textarea>
                                            </div>
                                            <button type="submit" class="btn btn-info">Submit</button>
                                        </form>
                                    </li>
                                    <li class="col-lg-3 col-xlg-4 m-b-30">
                                        <h4 class="m-b-20">List style</h4>
                                        <!-- List style -->
                                        <ul class="list-style-none">
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> You can give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another Give link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Forth link</a></li>
                                            <li><a href="javascript:void(0)"><i class="fa fa-check text-success"></i> Another fifth link</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- End mega menu -->
                        <!-- ============================================================== -->
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/users/1.jpg" alt="user" class="img-circle" width="30"></a>
                            <div class="dropdown-menu dropdown-menu-right user-dd animated flipInY">
                                <span class="with-arrow"><span class="bg-primary"></span></span>
                                <div class="d-flex no-block align-items-center p-15 bg-primary text-white m-b-10">
                                    <div class=""><img src="<?php echo PATH_IMAGE_NEW ?>/assets/images/users/1.jpg" alt="user" class="img-circle" width="60"></div>
                                    <div class="m-l-10">
                                        <h4 class="m-b-0">Steave Jobs</h4>
                                        <p class=" m-b-0">varun@gmail.com</p>
                                    </div>
                                </div>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> My Profile</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-wallet m-r-5 m-l-5"></i> My Balance</a>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-email m-r-5 m-l-5"></i> Inbox</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="javascript:void(0)"><i class="ti-settings m-r-5 m-l-5"></i> Account Setting</a>
                                <div class="dropdown-divider"></div>
                                <a id="logoff" href="index.php" class="dropdown-item" ><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                <div class="dropdown-divider"></div>
                                <div class="p-l-30 p-10"><a href="javascript:void(0)" class="btn btn-sm btn-success btn-rounded">View Profile</a></div>
                            </div>
                        </li>
                        <!-- ============================================================== -->
                        <!-- User profile and search -->
                        <!-- ============================================================== -->
                        <li class="nav-item right-side-toggle"> <a class="nav-link  waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li>
                    </ul>
                </div>
            </nav>
        </header>

















        
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <aside class="left-sidebar">
            <div class="d-flex no-block nav-text-box align-items-center">
                <span><img src="<?php echo PATH_IMAGE_NEW; ?>/assets/images/logo-icon.png" alt="elegant admin template"></span>
                <a class="nav-lock waves-effect waves-dark ml-auto hidden-md-down" href="javascript:void(0)"><i class="mdi mdi-toggle-switch"></i></a>
                <a class="nav-toggler waves-effect waves-dark ml-auto hidden-sm-up" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
            </div>
            <!-- Sidebar scroll-->
            <div class="scroll-sidebar">
                <!-- Sidebar navigation-->
                <nav class="sidebar-nav">
                    <ul id="sidebarnav">
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="icon-speedometer"></i><span class="hide-menu">Dashboard <span class="badge badge-pill badge-cyan">4</span></span></a>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu">Masters</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo MASTERS_LINK_CONTROL?>/district">District</a></li>
                                <li><a href="<?php echo MASTERS_LINK_CONTROL?>/block">Block</a></li>
                                <li><a href="<?php echo MASTERS_LINK_CONTROL?>/course">Course</a></li>
                                <li><a href="<?php echo MASTERS_LINK_CONTROL?>/employee">Employee</a></li>
                                <!-- <li><a href="app-calendar.html">Calendar <i class="ti-calendar"></i></a></li>
                                <li><a href="app-chat.html">Chat app <i class="ti-comment"></i></a></li>
                                <li><a href="app-ticket.html">Support Ticket <i class="ti-support"></i></a></li>
                                <li><a href="app-contact.html">Contact / Employee <i class="ti-user"></i></a></li>
                                <li><a href="app-contact2.html">Contact Grid <i class=" ti-list"></i></a></li>
                                <li><a href="app-contact-detail.html">Contact Detail <i class="ti-pencil-alt"></i></a></li> -->
                            </ul>
                        </li>
                        <li> <a class="has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false"><i class="ti-layout-grid2"></i><span class="hide-menu">Create Logins</span></a>
                            <ul aria-expanded="false" class="collapse">
                                <li><a href="<?php echo PATH_ADMIN_LINK?>/rm-login">Regional Manager Login</a></li>
                                <li><a href="<?php echo PATH_ADMIN_LINK?>/dm-login">District Manager Login</a></li>
                                <li><a href="<?php echo PATH_ADMIN_LINK?>/cm-login">Center Manager Login</a></li>
                                <!-- <li><a href="app-calendar.html">Calendar <i class="ti-calendar"></i></a></li>
                                <li><a href="app-chat.html">Chat app <i class="ti-comment"></i></a></li>
                                <li><a href="app-ticket.html">Support Ticket <i class="ti-support"></i></a></li>
                                <li><a href="app-contact.html">Contact / Employee <i class="ti-user"></i></a></li>
                                <li><a href="app-contact2.html">Contact Grid <i class=" ti-list"></i></a></li>
                                <li><a href="app-contact-detail.html">Contact Detail <i class="ti-pencil-alt"></i></a></li> -->
                            </ul>
                        </li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/students">Students</a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/notice">Notice</a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/news">News</a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/approved_regestration">Received Registraion Fees</a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/press_release">Press Release</a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/image_gallery">Image Gallery Upload</a></li> 
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/scholarship">Scholarship Exam  </a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/examcreate"> Exam Create </a></li>
                        <li><a href="<?php echo PATH_ADMIN_LINK?>/examresult"> Exam Result </a></li>

                        
                    </ul>
                </nav>
                <!-- End Sidebar navigation -->
            </div>
            <!-- End Sidebar scroll-->
        </aside>




















<!--

<div class="headPart noPrint">
  <div>
    <!--<div class="title flote_left"><img width="60" src="<?php echo PATH_IMAGE ?>/logo.png" alt="" /></div>
    <div class="pull-left" style="padding-left:20px;">
      <h2>Admin Panel</h2>
    </div>
    <div class="logout float_right">
      <div class="btn-toolbar">
        <div class="user_style">
          <button type="button" class="btn btn-default logoffBtn" id="logoff"> <span class="glyphicon glyphicon-off"></span> Logoff </button>
        </div>
        <div class="float_right">
          <div class="btn-group">
            <button class="btn btn-primary" style="padding:11px 10px 11px 10px"><span class="glyphicon glyphicon-user"></span></button>
            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle" style=" padding:16px 6px 15px 6px"> <span class="caret"> </span></button>
            <ul class="dropdown-menu">
              <li><a href="<?php echo PATH_ADMIN_LINK?>/change_password.php">Change Password</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="clear"></div>
</div>


<div class="clear"></div>
<nav class="navbar navbar-inverse" role="navigation" style="border-radius:0;">
<ul class="nav nav-pills dropdown open">
  <li class="active"><a href="<?php echo PATH_ADMIN_LINK?>/home.php">Dashboard</a></li>
  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="100">Masters <b class="caret"></b></a>
    <ul class="dropdown-menu">
      <li><a href="<?php echo MASTERS_LINK_CONTROL?>/district">District</a></li>
      <li><a href="<?php echo MASTERS_LINK_CONTROL?>/block">Block</a></li>
      <li><a href="<?php echo MASTERS_LINK_CONTROL?>/course">Course</a></li>
      <li><a href="<?php echo MASTERS_LINK_CONTROL?>/employee">Employee</a></li>
    </ul>
  </li>
  
  <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="100">Create Logins <b class="caret"></b></a>
  	<ul class="dropdown-menu">
    	<li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/rm-login">Regional Manager Login</a></li>
        <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/dm-login">District Manager Login</a></li>
        <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/cm-login">Center Manager Login</a></li>
    </ul>
  </li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/students">Students</a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/notice">Notice</a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/news">News</a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/approved_regestration">Received Regestraion Fees</a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/press_release">Press Release</a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/image_gallery">Image Gallery Upload</a></li>
  
 
    <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/scholarship">Scholarship Exam  </a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/examcreate"> Exam Create </a></li>
  <li class="dropdown"><a href="<?php echo PATH_ADMIN_LINK?>/examresult"> Exam Result </a></li>

</ul>
</div>-->
