<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of news
$getNews=$db->ExecuteQuery("SELECT * FROM schloarshipexamreg");

?>
<script type="text/javascript" src="jsfile.js" ></script>

<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

<div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Scholarship</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Scholarship</li>
          </ol>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>

        <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="main">
  <!-- <div class="page-title">
        <div>
            <div class="col-lg-5 pull-left"><h4><i class="glyphicon glyphicon-plus"></i> Serach</h4></div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div> -->
  
  
  <div class="clear formbgstyle">
    <form class="form-horizontal" role="form" id="insertNews" method="post">
      <div>
        <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="news">Regestration Id:<span>*</span>:</label>
          <div class="col-sm-4">
          <input type="text" name="regid" id="regid"  class="form-control input-sm txtarea" />
         </div>
        </div>
        
        
        <hr />
        <div class="form-group">
          <div class="col-sm-3"></div>
          <div class="col-sm-3">
            <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
            <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
          </div>
        </div>
      </div>
    </form>
    <div id="replace">
    <table class="table table-hover table-bordered" id="addedProducts">
      <thead>
        <tr class="success">
          <th>Sno.</th>
          <th>Reg Id</th>
          <th>Student Name</th>
          <th>Photo</th>
           <th>DOB</th>
          <th>Father Name</th>
          <th>Mother Name</th>
          <th>Caste</th>
          <th>Aadhaar No</th>
          <th>Education</th>
           <th>Course</th>
           <th>Transaction_Date</th>
          <th>Amount</th>
          <th>PaymentId</th>
          <th>PaymentStatus</th>
         <!-- <th>Action</th>-->
        </tr>
      </thead>
      <tbody>
        <?php 
            $i=1;
            foreach($getNews as $getNewsVal){ ?>
        <tr>
          <td><?php echo $i;?></td>
          <td><?php echo $getNewsVal['Reg_Id'];?></td>
          <td><?php echo $getNewsVal['Student_Name'];?></td>
           <td><img width="100%" src="<?php echo PATH_DATA_IMAGE."/scholar/thumb/".$getNewsVal['Photo'];?>" alt="" /></td>
          <td><?php echo $getNewsVal['DOB'];?></td>
          <td><?php echo $getNewsVal['Father_Name'];?></td>
          <td><?php echo $getNewsVal['Mother_Name'];?></td>
          <td><?php echo $getNewsVal['Caste'];?></td>
           <td><?php echo $getNewsVal['Aadhaar_No'];?></td>
           <td><?php echo $getNewsVal['Education'];?></td>
            <td><?php echo $getNewsVal['Course_Id'];?></td>
          <td><?php echo $getNewsVal['Transaction_Date'];?></td>
           <td><?php echo $getNewsVal['amount'];?></td>
           <td><?php echo $getNewsVal['PaymentId'];?></td>
           <td><?php echo $getNewsVal['PaymentStatus'];?></td>
          <!-- <td><button type="button" class="btn btn-danger btn-sm delete" id="<?php echo $getNewsVal['Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> Delete </button></td>-->
        </tr>
        <?php $i++;} ?>
      </tbody>
    </table>
    </div>
  </div>
</div>
        </div>
      </div>
    </div>



  </div>
</div>


