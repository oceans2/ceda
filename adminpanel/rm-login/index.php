<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();
$getrmlogin=$db->ExecuteQuery("SELECT R_Id, R_Emp_Code, R_State, R_District, State_Name, District_Name, Block_Name, R_Block, R_Address, R_Password, e.EMP_Name AS R_Name, CASE WHEN R_Status = 1 THEN 'Block' WHEN R_Status = 0 THEN 'Unblock' END R_Status
  FROM rm_login rm


  LEFT JOIN block_master b ON rm.R_Block = b.Block_Id
  LEFT JOIN district_master d ON rm.R_District = d.District_Id
  LEFT JOIN state_master s ON rm.R_State = s.State_Id
  LEFT JOIN employee_master e ON rm.R_Emp_Code = e.EMP_Code 
  ");


?>
<script type="text/javascript"  src="rm.js" ></script>


<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">RM Login List</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">RM Login List</li>
          </ol>
           <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;">
            <a class="btn btn-primary" href="add_rm.php" title="Add Course"><i class="glyphicon glyphicon-plus"></i> Add New RM Login</a>
          </span>
        </div>
      </div>
    </div>
    <div class="clearfix">&nbsp;</div>


    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
         <table class="table table-hover table-bordered" id="rmloginlist">
      <thead>
        <tr class="success">
          <th>Sno.</th>
          <th>RM Emp. Name</th>
          <th>Employee Code</th>
          <th>Block</th>
          <th>District</th>
          <th>State</th>
          <th>Office Address</th>
          <th>Action</th>
        </tr>
      </thead>
      <tbody>
       <?php
        if($getrmlogin)
        {
          $i=1;
          foreach($getrmlogin as $getrmloginVal)
          { ?>
            <tr>
            <td><?php echo $i;?></td>
            <!-- <td><?php echo $getrmloginVal['R_Name'].' - '.$getrmloginVal['R_Emp_Code'];?></td> -->
            <td><?php echo $getrmloginVal['R_Name'];?></td>
            <td><?php echo $getrmloginVal['R_Emp_Code'];?></td>
            <td><?php echo $getrmloginVal['Block_Name'];?></td>
            <td><?php echo $getrmloginVal['District_Name'];?></td>
            <td><?php echo $getrmloginVal['State_Name'];?></td>
            <td><?php echo $getrmloginVal['R_Address'];?></td>
          
            <td><button type="button" id="editbtn" class="btn btn-success btn-sm" onClick="window.location.href='edit_rm.php?id=<?php echo $getrmloginVal['R_Id'];?>'" > <span class="glyphicon glyphicon-edit"></span> Edit </button>
              <button type="button" class="btn btn-danger btn-sm delete" id="<?php echo $getrmloginVal['R_Id']; ?>" name="delete"> <span class="glyphicon glyphicon-trash"></span> Delete </button>
            
            <?php if($getrmloginVal['R_Status']==0){ ?>
            <button type="button" class="btn btn-warning btn-sm status" id="status-<?php echo $getrmloginVal['R_Id']; ?>" name="status"> <span class="glyphicon glyphicon-lock"></span> <?php echo $getrmloginVal['R_Status'] ?> </button>
            <?php }else{ ?>
              <button type="button" class="btn btn-primary btn-sm status" id="status-<?php echo $getrmloginVal['R_Id']; ?>" name="status"> <i class="fa fa-unlock" aria-hidden="true"></i> <?php echo $getrmloginVal['R_Status'] ?> </button>
            <?php } ?>
            </td>
            </tr>
          <?php $i++;} 
          }
          ?>
      </tbody>
    </table>
        </div>
      </div>
    </div>
  </div>
</div>


  <script>
    $(document).ready(function() {
      
        $('#rmloginlist').DataTable();   
    });
    
    </script>