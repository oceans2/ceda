<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
include(PATH_ADMIN_INCLUDE.'/header.php');
$db = new DBConn();

// get all list of news
$getNews=$db->ExecuteQuery("SELECT * FROM news WHERE News_Id=".$_GET['id']);

?>
<script type="text/javascript" src="news.js" ></script>


<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">

<div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Edit Exam</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Edit Exam</li>
          </ol>
          <!-- <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="view.php"><i class="glyphicon glyphicon-share-alt"></i> View  List</a></span> -->
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="main">
  <!-- <div class="page-title">
        <div>
            <div class="col-lg-5 pull-left"><h4><i class="glyphicon glyphicon-edit"></i> Edit News</h4></div>
        </div>
        <div class="clearfix">&nbsp;</div>
    </div> -->
  
  
  <div class="clear formbgstyle">
    <form class="form-horizontal" role="form" id="editNews" method="post">
      <div>
        <div class="form-group">
          <label class="control-label col-sm-3 mandatory" for="news">News:<span>*</span>:</label>
          <div class="col-sm-4">
            <textarea  class="form-control input-sm txtarea" id="news" name="news" placeholder="Training Center Address"><?php echo $getNews[1]['News_Content']; ?></textarea>
          </div>
        </div>
        
        
        <div class="form-group">
          <div class="col-sm-3"></div>
          <div class="col-sm-3">
            <input type="hidden" id="Newsid" value="<?php echo $getNews[1]['News_Id']; ?>">
            <input type="button" class="btn btn-primary btn-sm" id="edit" value="Update">
            <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
          </div>
        </div>
      </div>
    </form>
  </div>
</div>
        </div>
      </div>
    </div>




  </div>
</div>

