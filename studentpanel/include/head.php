

<?php 
/*$newURL= "http://sdmsonline.in/";
header('Location: '.$newURL);*/
?>
 

 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
    <title>CHHATTISGARH EDUCATION DEVELOPMENT ASSOCIATION(CEDA)</title>
    
    <!-- page css -->
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/dist/css/pages/login-register-lock.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/dist/css/style.min.css" rel="stylesheet">
    
    <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES; ?>/js/jquery-1.10.2.js"></script>
    <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/jquery/jquery-3.2.1.min.js"></script>
    

    

    

    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
    <!--Toaster Popup message CSS -->
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/assets/node_modules/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!--c3 plugins CSS -->
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/assets/node_modules /c3-master/c3.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/dist/css/style.min.css" rel="stylesheet">
    <!-- Dashboard 1 Page CSS -->
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/css/dist/css/pages/dashboard1.css" rel="stylesheet">


    <!-- Bootstrap popper Core JavaScript -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/popper/popper.min.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- slimscrollbar scrollbar JavaScript -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/dist/js/perfect-scrollbar.jquery.min.js"></script>
  <!--Wave Effects -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/dist/js/waves.js"></script>
  <!--Menu sidebar -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/dist/js/sidebarmenu.js"></script>
  <!--Custom JavaScript -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/dist/js/custom.min.js"></script>
  <!-- ============================================================== -->
  <!-- This page plugins -->
  <!-- ============================================================== -->
  <!--morris JavaScript -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/raphael/raphael-min.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/morrisjs/morris.min.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/jquery-sparkline/jquery.sparkline.min.js"></script>
  <!--c3 JavaScript -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/d3/d3.min.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/c3-master/c3.min.js"></script>
  <!-- Popup message jquery -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/toast-master/js/jquery.toast.js"></script>
  <!-- Chart JS -->
  <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/dist/js/dashboard1.js"></script>

  <script src="<?php echo PATH_CSS_LIBRARIES ?>/css/assets/node_modules/wizard/jquery.validate.min.js"> </script>

  <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES; ?>/js/jquery.validate.js"></script>
    <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES; ?>/js/additional-methods.js"></script>

    <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/sticky-kit-master/dist/sticky-kit.min.js"></script>
    <script src="<?php echo PATH_JS_LIBRARIES; ?>/css/assets/node_modules/datatables/jquery.dataTables.min.js"></script>

    <script>
    jQuery.extend(jQuery.validator.messages, {
    required: "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>This field is required.</strong></div>" ,
    number: "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Please enter a valid number.</strong></div>",
    digits: "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Please enter only digits.</strong></div>",
    email:  "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Please enter a valid email address.</strong></div>",
    date: "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Please enter a valid date.</strong></div>",
    maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
    minlength: jQuery.validator.format("Please enter at least {0} characters."),
    rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
    range: jQuery.validator.format("Please enter a value between {0} and {1}."),
    max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
    min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
});
    </script>

   </head> 