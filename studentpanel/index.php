<?php 
include('../config.php');
require_once(PATH_STUDENT_INCLUDE.'/header.php');
?>

<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Admit Card</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Admit Card</li>
          </ol>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        
        <div class="x_content">
          <div class="col-sm-3 text-center dashboard-icons text-success"><i class="fa fa-user" aria-hidden="true"></i> <p><a href="my-profile.php">My Profile</a></p></div>
            <div class="col-sm-3 text-center dashboard-icons text-warning"><i class="fa fa-thumb-tack" aria-hidden="true"></i> <p><a href="center-notice.php">Center Notice</a></p></div>
            <div class="col-sm-3 text-center dashboard-icons text-warning"><i class="fa fa-thumb-tack" aria-hidden="true"></i> <p><a href="ho-notice.php">Head Office Notice</a></p></div>
            <!--<div class="col-sm-3 text-center dashboard-icons text-info"><i class="fa fa-upload" aria-hidden="true"></i> <p><a href="upload-document/index.php">Upload Documents</a></p></div>-->
            <div class="col-sm-3 text-center dashboard-icons text-primary"><i class="fa fa-file-text" aria-hidden="true"></i> <p><a href="">Admit Card</a></p></div>
            <div class="col-sm-3 text-center dashboard-icons text-danger"><i class="fa fa-inr" aria-hidden="true"></i> <p><a href="fees.php">Fees Details</a></p></div>
            <div class="col-sm-3 text-center dashboard-icons text-primary"><i class="fa fa-key" aria-hidden="true"></i> <p><a href="">Change Password</a></p></div>
            
            
        </div>
      </div>
    </div>
  </div>
        </div>
      </div>
    </div>



  </div>
</div>

<?php 
require_once(PATH_STUDENT_INCLUDE.'/footer.php');

?>



