<?php 
include('../config.php');

require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();

require_once(PATH_STUDENT_INCLUDE.'/header.php');

$GetNotice = $db->ExecuteQuery("SELECT DATE_FORMAT(Notice_Date,'%d-%M-%Y') AS Notice_Date, Notice

FROM ho_notice ORDER BY Notice_Id DESC");
?>

<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Head Office Notice</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Head Office Notice</li>
          </ol>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>



    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
      <div class="x_panel">
        
        <div class="x_content">

                  
                  <div>
    <?php foreach($GetNotice as $GetNoticeVal){ ?>
                    <div class="profile_title">
                      <div style="padding-left:10px;">
                        <h2><?php echo $GetNoticeVal['Notice_Date']; ?></h2>
                      </div>
                    </div>
                    <!-- start of user-activity-graph -->
                    <div id="graph_bar" style="margin-top:20px; padding-left:10px; padding-right:10px; padding-bottom:10px;">
                      <div>
                          <?php echo $GetNoticeVal['Notice']; ?>
                        </div>
                    </div>
                    <!-- end of user-activity-graph -->
          <?php } ?>
                    
                  </div>
                </div>
        
      </div>
    </div>
  </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once(PATH_STUDENT_INCLUDE.'/footer.php'); ?>

