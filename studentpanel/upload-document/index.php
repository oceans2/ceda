<?php
include('../../config.php'); 
require_once(PATH_LIBRARIES.'/classes/DBConn.php');
$db = new DBConn();

include(PATH_STUDENT_INCLUDE.'/header.php');

?>

<script type="text/javascript" src="document.js"></script>





<div id="loading">
    <div class="loader-block"><i class="fa-li fa fa-spinner fa-spin spinloader"></i></div>
</div>


<div class="page-wrapper">
  <!-- ============================================================== -->
  <!-- Container fluid  -->
  <!-- ============================================================== -->
  <div class="container-fluid">
    <div class="row page-titles">
      <div class="col-md-5 align-self-center">
        <h4 class="text-themecolor">Upload Document</h4>
      </div>
      <div class="col-md-7 align-self-center text-right">
        <div class="d-flex justify-content-end align-items-center">
          <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
            <li class="breadcrumb-item active">Upload Document</li>
          </ol>
          <!-- <span><a class="btn btn-primary" href="list.php"><i class="glyphicon glyphicon-share-alt"></i> View Document List</a></span> -->
          <span class="hidden-phone" style="margin-left: 15px; margin-top: 3px;"><a class="btn btn-primary" href="list.php"><i class="glyphicon glyphicon-share-alt"></i> View Document List</a></span>
          <!-- <button type="button" class="btn btn-success d-none d-lg-block m-l-15"><i class="fa fa-plus-circle"></i> Create New</button> -->
        </div>
      </div>
    </div>

    <div class="card-group">
      <div class="card o-income">
        <div class="card-body">
          <div class="x_content">
          <form class="form-horizontal" role="form" id="insertDoc" method="post">
              <div>
                <div class="form-group">
                  <label class="control-label col-sm-4 mandatory" for="doc_name">Document Name :</label>
                  <div class="col-sm-5">
                    <input type="text" class="form-control" id="doc_name" name="doc_name" placeholder="Ex: Voter Id Card" />
                  </div>
                </div>
                
                <div class="item form-group">
                  <label class="control-label col-md-4 col-sm-4 col-xs-12" for="fileupload">Updoad Document <span class="required">*</span> </label>
                  <div class="col-md-5 col-sm-5 col-xs-12">
                        <input type="file" id="fileupload" name="fileupload" required="required" class="form-control col-md-7 col-xs-12" accept="pdf">
                        <span id="errmsg"></span> (Note : Only JPG, PNG, GIF Can Upload.) </div>
                  </div>
                
                <hr />
                
                <div class="form-group">
                  <div class="col-sm-4"></div>
                  <div class="col-sm-3">
                    <input type="button" class="btn btn-primary btn-sm" id="submit" value="Submit">
                    <input type="reset" class="btn btn-default btn-sm" id="reset" value="Reset">
                  </div>
                </div>
              </div>
            </form>
        </div>
        </div>
      </div>
    </div>

  </div>
</div>
<?php require_once(PATH_STUDENT_INCLUDE.'/footer.php'); ?>

