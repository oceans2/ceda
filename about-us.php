<?php include('config.php'); 
include('header.php'); ?>

<!-- Back to Top Script-->
<script>
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.back-to-top').fadeIn(duration);
			} else {
				jQuery('.back-to-top').fadeOut(duration);
			}
		});
		
		jQuery('.back-to-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		});
		
		
	});
</script>
<!-- eof Back to Top Script-->

        <!--eof header ** homeMid starts from here-->
        <div class="container homeMid">
        	<div>
                <div class="page-content container">
                	<h1>About Us</h1>
                    
                    <div class="container" >
                        <article class="welcome">
                          
                                <h5>OUR AIM</h5>
                                <p>Introducing NSDC, Bringing financial literacy and low-cost credit to doorstep of rural india through a revolutionary bioscope.</p>
                                
                                <h5>VISSION</h5>
                                <p>No one in INDIA shall be deprived of working skills becouse of lack of education.</p>
                                
                                <h5>MISSION</h5>
                                <p>To educate with the help of NSDC training programes 5 million people by 2020.</p>
                                
                                <h5>DIRECTOR’S DESK</h5>
                                <p>I don't believe in charity, I believe in solidarity. Charity is vertical so it is humiliating; it goes top to the bottom. Solidarity is horizontal, It respect the other and learns from the other I have a lot of learn from the other people, some people are unemployed because they lack the minimum required education, others are unemployed because they lack the minimum required obedience in life, no one and nothing will help you until you start helping yourself. Listen to your teacher when they tell you what to do but more importantly think about it later ask yourself why they told you to do it. Everyone has talent and do you, let it shine out, is all you have to do?</p>
                                
                                <h5>PROJECTS</h5>
                                <ul>
                                    <li>DDUGKY</li>
                                    <li>PMKVY</li>
                                    <li>STEP</li>
                                    <li>MSJ SCHEAME’S</li>
                                    <li>NDLM</li>
                                    <li>MSME</li>
                                    <li>MSDE</li>
                                    <li>NIESBUD</li>
                                    <li>CSC</li>
                                </ul>
                                
                                <h5>OUR SERVICES</h5>
                                <ul>
                                    <li>Assistance in fund raising for NGOs.</li>
                                    <li>We provided door step services like-(Submitted 27 & 28 acts, Annual Auditing &      Annual balance sheet.)</li>
                                    <li>Preparation of Government & non-Gov. projects.</li>
                                    <li>Consultancy for NGO management.</li>
                                    <li>Document preparation for new NGOs Registration.</li>
                                    <li>Document preparation for12 AA & 80G, 35AC & 35(1) (i) & FCRA. ,iso 2000:2009 certification</li>
                                    <li>Preparation Annual Report, Annual Audit of NGOs.</li>
                                    <li>Guidance for proper execution of projects sanctioned.</li>
                                    <li>New projects setups-</li>
                                </ul>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>School</td>
                                        <td>Old Edge home</td>
                                    </tr>
                                    <tr>
                                        <td>Colleges</td>
                                        <td>Training Institute</td>
                                    </tr>
                                    <tr>
                                        <td>Hospitals</td>
                                        <td>Paramedical colleges</td>
                                    </tr>
                                    <tr>
                                        <td>B.ed. Colleges</td>
                                        <td>Nurshing Colleges</td>
                                    </tr>
                                    <tr>
                                        <td>Go - Shala</td>
                                        <td>Anath Ashram</td>
                                    </tr>
                                    <tr>
                                        <td>ITI</td>
                                        <td>VTP CENTER’S</td>
                                    </tr>
                                </table>
                                
                                <p><strong>Check ist of Documents required for drafting project application for NSDC and many other Government Grants and to be submitted</strong></p>
                                
                                <table class="table table-bordered">
                                    <tr class="info">
                                        <td>Sno.</td>
                                        <td>Particulars</td>
                                    </tr>
                                    <tr>
                                        <td>1</td>
                                        <td>Proof of Registration (Certificate of Registration, or Regd. Trust Deed)</td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>Memorandum & Articles of NGO and bye laws</td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>Last 3 years Annual Accounts duly audited by CA</td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>Last 3 years Annual Reports</td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>List of Managing Committee Members (Governing Body)</td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>List of staff/Employees in NGO</td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>Bank Account details of NGO- One canceled Cheque and Last one year bank statement</td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>Contact details of NGO like Telephone No., E-mail ID.</td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>Activities photograph</td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>12AA Certificate/Application filing receipt</td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>All Members Profile</td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>All Members PAN Card & Address Proof</td>
                                    </tr>
                                    <tr>
                                        <td>13</td>
                                        <td>One Page detailed information regarding project area</td>
                                    </tr>
                                    <tr>
                                        <td>14</td>
                                        <td>Pan card of NGO</td>
                                    </tr>
                                    <tr>
                                        <td>15</td>
                                        <td>List of Beneficiaries</td>
                                    </tr>
                                    <tr>
                                        <td>16</td>
                                        <td>Rent Agreement - Certified by competent authority</td>
                                    </tr>
                                    <tr>
                                        <td>17</td>
                                        <td>A Building Plan - Blue Print( In case Swadhar Project)</td>
                                    </tr>
                                    <tr>
                                        <td>18</td>
                                        <td>Rent assesment certificate -Issued by state PWD/District Collector/Municipal Authorities.( In case Swadhar Project)</td>
                                    </tr>
                                    <tr>
                                        <td>19</td>
                                        <td>Application in prescribed format for and Proposal for financial assistance duly authenticated by Secretary, chairman and other officers of NGO.</td>
                                    </tr>
                                </table>
                                
                                <p style="font-size:1.5em" class="text-danger"><strong>We appreciate your stated interest in continuing partnership with me to build ecosystem for skill development</strong></p>
                                <p>
                        	RURAL INSTITUTE FOR CAREER AND EMPLOYMENT SOCIETY (RICEs)<br />
                            Kaledonia 1st Floor, Office No-26<br />
                            Sahar Road , Andheri East<br />
                            Mumbai 400069<br /><br />
                            
                            <strong>In case of any clearance please feel free to call me or mail me if necessary any also. </strong><br />
                            <strong>Mobile</strong>:	9522223699<br />
                            <strong>Landline</strong>: 02266117962<br />
                            <strong>Email</strong> : <a href="mailto:support@riceedu.org">support@riceedu.org</a><br />
                            <strong>Visit us on</strong> : <a target="_blank" href="http://www.riceedu.org">www.riceedu.org</a><br /><br />
                        </p>
        
                            
                        </article><!--//page-content-->
                        
                       
                        
                    </div>
                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--eof homeMid ** footer starts from here-->
        <a href="#" class="back-to-top">&nbsp;</a>
<?php include('footer.php'); ?>