// JavaScript Document
$(document).ready(function(){
	
	$("#msg").hide();
	$("#msg2").hide();
	
	///////////////////////////////////
	// Add Employee form validation
	////////////////////////////////////
	$("#admitcardform").validate({
		rules: 
		{
			regid: 
			{ 
				required: true,
				
			},
		},
		messages:
		{
			
		}
	});// eof validation
	$("#resultform").validate({
		rules: 
		{
			regestrationidresult: 
			{ 
				required: true,
				
			},
		},
		messages:
		{
			
		}
	});// eof validation
	
	
	///////////////////////////////////////////////////
	// Method to check is the data already exist or not
	///////////////////////////////////////////////////
	/*$.validator.addMethod('validEmpCode', function(val, element)
	{		
		$.ajax({
			 url:"login_curd.php",
			 type: "POST",
			 data: {type:"validEmpCode",emp_code:$("#emp_code").val()},
			 async:false,
			 success:function(data){ //alert(data);
				 isSuccess=(data==1)?true:false;
			 }
		});//eof ajax
		return isSuccess ;				
	}, 'Invalid Employee Code.');*/

	
	//////////////////////////////////
	// on click of FindEmpBtn button
	//////////////////////////////////
	
	///////////////////////////////////
	// RM Form Validation
	////////////////////////////////////
	
	
	//////////////////////////////////
	// on click of FindEmpBtn button
	//////////////////////////////////
	$("#admitcard").click(function()
	{
		
		flag=$("#admitcardform").valid();
		
		if (flag==true)
		{
			$("#msgadmit").text('');
			var regid =$("#regid").val();
			
			var x;
			$.ajax(
			{
				url:'admitcardcurdfile.php',
				type:'POST',
				data:{type:"check", regid:regid},
				success:function(data){ 
				if(data==1)
				{
					 window.location.replace("admitcard.php?regid="+regid);
				}
				else
				{
					$("#msgadmit").text('Sorry Admit Card Not Uploaded Yet');
				}
					
				}
			});
			
		}//eof if condition
		
	});//eof click event
		
	
	///////////////////////////////////
	// DM Form Validation
	////////////////////////////////////
	//////////////////////////////////
	// on click of FindEmpBtn button
	//////////////////////////////////
	$("#resultbutton").click(function()
	{
		flag=$("#resultform").valid();
		var regid =$('#regestrationidresult').val();
		
		if (flag==true)
		{			
			
			var x;
			$.ajax(
			{
				url:'admitcardcurdfile.php',
				type:'POST',
				data:{type:"result",regid:regid},
				success:function(data){ 
				//alert(data)
				if(data==1)
				{
					 window.location.replace("result.php?regid="+regid);
				}
				else
				{
					$("#msgresult").text('Sorry Result Not Uploaded Yet');
				}
					
				}
			});
			
		}//eof if condition
	});//eof click event
	
	
	

});