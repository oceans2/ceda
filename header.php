<!DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>RICE Institute of Computer Education</title>
    
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/assets/node_modules/bootstrap/dist/js/bootstrap.min.js" rel="stylesheet" type="text/css" />
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/bootstrap-theme.min.css" rel="stylesheet"  type="text/css">
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/style.css" rel="stylesheet"  type="text/css"/>
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/sub_menu.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="<?php echo PATH_CSS_LIBRARIES ?>/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo PATH_CSS_LIBRARIES ?>/css/owl.carousel.css">
    <link rel="stylesheet" href="<?php echo PATH_CSS_LIBRARIES ?>/css/owl.theme.default.min.css" />
    <link href="<?php echo PATH_CSS_LIBRARIES ?>/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo PATH_CSS_LIBRARIES ?>/css/jquery-ui.css">
    <script src="<?php echo PATH_JS_LIBRARIES ?>/js/jquery.js" type="text/javascript"></script>
    <script src="<?php echo PATH_JS_LIBRARIES ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?php echo PATH_JS_LIBRARIES ?>/js/owl.carousel.min.js"></script>
    <script src="<?php echo PATH_JS_LIBRARIES ?>/js/jquery-ui.js"></script>

      <!-- daterangepicker -->
  <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES ?>/js/moment/moment.min.js"></script>
  <script type="text/javascript" src="<?php echo PATH_JS_LIBRARIES ?>/js/bootstrap-datetimepicker.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/custom.js"></script>
  <script src="<?php echo PATH_JS_LIBRARIES ?>/js/jquery.validate.js"></script>
  
   <script type="text/javascript">
   <!--Time Picker Show here -->
	$(function () {
		$('.datetimepicker').datepicker({
			orientation: "top auto",
			changeYear:true,
			forceParse: false,
			autoclose: true,
			dateFormat: 'dd-mm-yy'
		});
	});
	<!--Datepicker Show here-->
 </script>
</head>

<body>
	<div class="container-fluid">
    	<div class="topStrip">
            <div class="container text-right">
                <ul>
                    <li><a href="<?php echo PATH_ROOT ?>/contact_us.php"><i class="glyphicon glyphicon-map-marker"></i> Contact</a></li>
                    <li><a href="<?php echo PATH_ROOT ?>/important-notice.php"><i class="glyphicon glyphicon-volume-up"></i> Important Notice</a></li>
                </ul>
            </div>
        </div>
        <!--header starts from here-->
        <header>
        	<hgroup>
            	<div class="text-center"><a href="<?php echo PATH_ROOT ?>/index.php"><img class="img-responsive" src="<?php echo PATH_IMAGE ?>/gsclogo.png" alt=""></a></div>
            	<div class="darkgray-strip">
                	<div class="container">
                    	<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <nav class="site-navigation collapse navbar-collapse bs-navbar-collapse">
                        	<ul>
                            	<li><a href="<?php echo PATH_ROOT ?>/index.php"><i class="glyphicon glyphicon-home"></i> Home</a></li>
                                <li><a href="<?php echo PATH_ROOT ?>/about-us.php"><i class="glyphicon glyphicon-star-empty"></i> About Us</a></li>
                                <li><a href="<?php echo PATH_ROOT ?>/knowledge-bank.php"><i class="glyphicon glyphicon-map-marker"></i> Knowledge Bank</a></li>
                                
                                <!--<li><a href="<?php echo PATH_ROOT ?>/press_release.php"><i class="glyphicon glyphicon-map-marker"></i> Press Release</a></li>-->

                                <li><a href="<?php echo PATH_ROOT ?>/under-construction.php"><i class="glyphicon glyphicon-asterisk"></i> Our Placement</a></li>
                                <li><a href="<?php echo PATH_ROOT ?>/under-construction.php"><i class="glyphicon glyphicon-plus"></i> Our Product</a></li>
                                <li><a href="<?php echo PATH_ROOT ?>/scholarship-exam.php"><i class="glyphicon glyphicon-list-alt"></i> Scholarship Exam</a></li>
                                
                                <li><a href="<?php echo PATH_ROOT ?>/image_gallery.php"><i class="glyphicon glyphicon-picture"></i> Image Gallery</a></li>
                                <li><a href="<?php echo PATH_ROOT ?>/login.php"><i class="glyphicon glyphicon-log-in"></i> login</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </hgroup>
        </header>