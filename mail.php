<?php 
require_once "DBConn.php";

class mail extends DBConn
{
	
	function FaildMail($id)
	{
		
		
		
		//*********************************
		// Get reservation details from DB
		//*********************************
		$bookingInfo = $this->ExecuteQuery("SELECT * FROM schloarshipexamreg WHERE Reg_Id='".$id."'");
		
		
		//************************************************************
	
		
		//*********************************
		// User Email Id //////////////////
		//*********************************		
		//$to  = "billing@vanvinodan.com";
		$to  = $bookingInfo[1]['Email'];
		
		//*********************************************************
		// Email Subject //////////////////////////////////////////
		//*********************************************************
		$subject = 'Invalid Transaction ,Transaction Failed';
		//*********************************************************
		// Message ////////////////////////////////////////////////
		//*********************************************************
		$message = '
		<table width="600" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td style="padding:20px; background:#F2F2F2;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                        <td><img src="images/logo.png" alt="" /></td>
                    </tr>
                    <tr>
                    	<td style="background:#fff; padding:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            	<tr>
                                	<td>
                                    	<h2> In  Rural Institute For Career And Employment Society	For Scholarship Exam  Your Transaction  Is Failed</h2>
                                        <p>your payment details</p>
                                    </td>
                                </tr>
                                <tr>
                                	<td style="background:#F2F2F2; padding:10px;">
                                    	YOUR  DETAILS
                                    </td>
                                </tr>
                                <tr>
                                	<td style="padding:20px 10px; border:solid 1px #F2F2F2;">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        	<tr>
                                            	<td><strong>Regestration  No.:</strong> '.$bookingInfo[1]['Reg_Id'].'</td>
                                                <td style="text-align:right;"><strong>Date:</strong> '.date("M d, Y").'</td>
                                          
                                            	<td><strong>Transaction No.:</strong> '.$bookingInfo[1]['Transaction_Id'].'</td>
                                               
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
		
		//*********************************************************
		// To send HTML mail, the Content-type header must be set
		//*********************************************************
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		//*********************************************************
		// Additional headers /////////////////////////////////////
		//*********************************************************
		$headers .= 'From: 	 rural institute for career and employment society <admin@riceedu.org>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		//*********************************************************
		// Send Mail //////////////////////////////////////////////
		//*********************************************************
		mail($to, $subject, $message, $headers);
		//*********************************************************
	}//eof billing function
	
	
	
	//////////////////////////////////////////////////////////////////////////////
	/// Function to send the pending reservation mail to user from billing ///////
	//////////////////////////////////////////////////////////////////////////////
	function ConfirmedMail($reservationId){
		
		//*********************************
		// Get reservation details from DB
		//*********************************
		$bookingInfo = $this->ExecuteQuery("SELECT * FROM schloarshipexamreg WHERE Reg_Id='".$reservationId."'");
		
		
		//************************************************************
	
		
		//*********************************
		// User Email Id //////////////////
		//*********************************		
		//$to  = "billing@vanvinodan.com";
		$to  = $bookingInfo[1]['Email'];
		
		//*********************************************************
		// Email Subject //////////////////////////////////////////
		//*********************************************************
		$subject = 'Confirmation of Scholarship Exam';
		//*********************************************************
		// Message ////////////////////////////////////////////////
		//*********************************************************
		$message = '
		<table width="600" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td style="padding:20px; background:#F2F2F2;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                        <td><img src="images/logo.png" alt="" /></td>
                    </tr>
                    <tr>
                    	<td style="background:#fff; padding:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            	<tr>
                                	<td>
                                    	<h2>Thanks for  Regestration In  Rural Institute For Career And Employment Society	For Scholarship Exam  Your Transaction  Is Compleate</h2>
                                        <p>your payment details</p>
                                    </td>
                                </tr>
                                <tr>
                                	<td style="background:#F2F2F2; padding:10px;">
                                    	YOUR  DETAILS
                                    </td>
                                </tr>
                                <tr>
                                	<td style="padding:20px 10px; border:solid 1px #F2F2F2;">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        	<tr>
                                            	<td><strong>Regestration  No.:</strong> '.$bookingInfo[1]['Reg_Id'].'</td>
                                                <td style="text-align:right;"><strong>Date:</strong> '.date("M d, Y").'</td>
                                          
                                            	<td><strong>Transaction No.:</strong> '.$bookingInfo[1]['Transaction_Id'].'</td>
                                               
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
		
		//*********************************************************
		// To send HTML mail, the Content-type header must be set
		//*********************************************************
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		//*********************************************************
		// Additional headers /////////////////////////////////////
		//*********************************************************
		$headers .= 'From: 	 rural institute for career and employment society <admin@riceedu.org>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		//*********************************************************
		// Send Mail //////////////////////////////////////////////
		//*********************************************************
		mail($to, $subject, $message, $headers);
		//*********************************************************
	}//eof billing function
		
		function Scholarshipregestration($id)
		{
			//*********************************
		// Get reservation details from DB
		//*********************************
		$bookingInfo = $this->ExecuteQuery("SELECT * FROM schloarshipexamreg WHERE Reg_Id='".$reservationId."'");
		
		
		//************************************************************
	
		
		//*********************************
		// User Email Id //////////////////
		//*********************************		
		//$to  = "billing@vanvinodan.com";
		$to  = $bookingInfo[1]['Email'];
		
		//*********************************************************
		// Email Subject //////////////////////////////////////////
		//*********************************************************
		$subject = 'Confirmation of Scholarship Exam';
		//*********************************************************
		// Message ////////////////////////////////////////////////
		//*********************************************************
		$message .= '
		<table width="600" border="0" cellspacing="0" cellpadding="0">
    	<tr>
        	<td style="padding:20px; background:#F2F2F2;">
            	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                	<tr>
                        <td></td>
                    </tr>
                    <tr>
                    	<td style="background:#fff; padding:15px; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
                        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            	<tr>
                                	<td>
                                    	<h2>Thank you for registring with RICE SOCIETY</h2>
										<h2>Welcome to RURAL INSTITUTE FOR CAREER & EMPLOYMENT SOCIETY</h2>
                                      
                                    </td>
                                </tr>
                                <tr>
                                	<td style="background:#F2F2F2; padding:10px;">
                                    	YOUR  DETAILS
                                    </td>
                                </tr>
                                <tr>
                                	<td style="padding:20px 10px; border:solid 1px #F2F2F2;">
                                    	<table width="100%" border="0" cellspacing="0" cellpadding="5">
                                        	<tr>
                                            	<td><strong>Regestration  No.:</strong> '.$bookingInfo[1]['Reg_Id'].'</td>
                                                <td style="text-align:right;"><strong>Date:</strong> '.date("M d, Y").'</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>';
		$message .= 'Best Regards, RURAL INSTITUTE FOR CAREER & EMPLOYMENT SOCIETY';
		$message .= 'CONTACT - 9329057958/9522223698';
		$message .='email - support@riceedu.org visit us -  http://www.riceedu.org';
		//*********************************************************
		// To send HTML mail, the Content-type header must be set
		//*********************************************************
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		//*********************************************************
		// Additional headers /////////////////////////////////////
		//*********************************************************
		$headers .= 'From: 	 rural institute for career and employment society <admin@riceedu.org>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		//*********************************************************
		// Send Mail //////////////////////////////////////////////
		//*********************************************************
		mail($to, $subject, $message, $headers);
		}
}

?>