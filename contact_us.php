<?php include('config.php'); 
include('header.php'); 

if (isset($_REQUEST['submit']) ) {
	
	require_once('recaptchalib.php');
  $privatekey = "6LennPISAAAAAJBERy3ksjlQllAOCn2K7uy9ckC5";
  $resp = recaptcha_check_answer ($privatekey,
		$_SERVER["REMOTE_ADDR"],
		$_POST["recaptcha_challenge_field"],
		$_POST["recaptcha_response_field"]);

  if (!$resp->is_valid) {
 	?>
	
    <script type="text/javascript">
    	$(document).ready(function(){
			$("#captchaError").show();
			$("#captchaError").html("<p class='alert alert-danger' style='margin-top:15px;'>The reCAPTCHA wasn't entered correctly. Please try it again.</p>")
		});// eof ready function
    </script>
    
    <?php
	
  }
  else{
	
		///////////////////////////////////
		//data send to the admin via Email
		///////////////////////////////////
		
		$to  = "support@riceedu.org";
		
		// subject ///////////////////////////////////////
		$subject = 'A New Enquiry has been initiated';
		
		$user_name=$_POST['name'];
		$emailId=$_POST['emailId'];
		$countrycode=$_POST['countrycode'];
		$phone=$_POST['phone'];
		$msg=$_POST['msg'];
		
		
		// message ////////////////////////////////////////////////
		$message = "
		<table width='100%' border='0' cellspacing='0' cellpadding='0'>
		  <tr>
			<td>Name:</td>
			<td align='left'><strong>$user_name</strong></td>
		  </tr>
		  <tr>
			<td>Email:</td>
			<td align='left'><strong>$emailId</strong></td>
		  </tr>
		  <tr>
			<td>Phone:</td>
			<td align='left'><strong>$countrycode-$phone</strong></td>
		  </tr>
		  <tr>
			<td>Message/Enquiry:</td>
			<td align='left'><strong>$msg</strong></td>
		  </tr>
		</table>";
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		
		
		// Additional headers
		$headers .= 'From: RICE EDU <contact@riceedu.org>' . "\r\n";
		//$headers .= 'Cc: birthdayarchive@example.com' . "\r\n";
		//$headers .= 'Bcc: birthdaycheck@example.com' . "\r\n";
		
		// Mail it
		mail($to, $subject, $message, $headers);
		////////////////////////////////////////////////////////////////
		echo 1;
  }//eof else
	
}// eof submit

?>
<script src="js/jquery.validate.js" type="text/javascript"></script>
<script src="js/contact.js" type="text/javascript"></script>


        <!--eof header ** homeMid starts from here-->
        <div class="container homeMid">
        	<div>
                <div class="page-content2">
                    <h1>Contact Us</h1>
                    <p class="directorMsg">
                        <strong>We appreciate your stated interest in continuing partnership with me to build ecosystem for skill development.</strong>
                    </p>
                </div>

                <div>
                    <div class="col-sm-6">
                        <div class="section">
                            <h3>HEAD OFFICE</h3>
                            <hr>
                            <h4><strong>RURAL INSTITUTE FOR CAREER & EMPLOYMENT SOCIETY</strong></h4>
                            <p>
                                OLD SARANGARH BUS STAND BEHIND KANHA HOSPITAL WARD NO. 31<br>
                                RAIGARH (C.G.) - 496001<br>
                                <strong>TIMING</strong> - 9: 30  AM TO 5:30 PM LUNCH TIME 1:00 PM TO 1:30 PM (Monday to
Saturday)<br>
                                <strong>FOR ENQUARY</strong> – 07762222569, 9329057958<br>
                                <strong>EMAIL</strong> - <a href="mailTo:support@riceedu.org">support@riceedu.org</a><br>
                                <strong>WEBSITE</strong> - <a target="_blank" href="http://www.riceedu.org">www.riceedu.org</a>
                            </p>
                            
                            <h4><strong>ADMINISTRATION DEPARTMENT</strong></h4>
                            <p>
                                <strong>CONTACT</strong> – 9522223698 
                                <strong>EMAIL</strong> - <a href="mailTo:admin@riceedu.org">admin@riceedu.org</a><br>
                            </p>

                            <h4><strong>EXAM DEPARTMENT</strong></h4>
                            <p>
                                <strong>CONTACT</strong> – 07762 215448
                                <strong>EMAIL</strong> - <a href="mailTo:exam@riceedu.org">exam@riceedu.org</a><br>
                            </p>
                        </div>
                    </div>
                    
                    <div class="col-sm-6">
                        <div class="section">
                            <h3>REGIONAL OFFICE MUMBAI</h3>
                            <hr>
                            <h4><strong>MR. S. SEHGAL</strong></h4>
                            <p>                                
                                OFFICE NO. 11 SUNSINE PARK BLDG EVERSINE NAGAR<br>
                                NEAR STATION NALASOPARA EAST <br>
                                MUMBAI MAHARASTRA - 401209<br>
                                <strong>TIMING</strong> - 10:AM TO 8: PM (Monday to Saturday)<br>
                                <strong>FOR ENQUARY</strong> – 9522223688, 9607883377<br>
                                <strong>EMAIL</strong> - <a href="mailTo:mumbairo@riceedu.org">mumbairo@riceedu.org</a><br>
                                <strong>WEBSITE</strong> - <a target="_blank" href="http://www.riceedu.org">www.riceedu.org</a>
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="section">
                            <h3>REGIONAL OFFICE UTTAR PRADESH</h3>
                            <hr>
                            <h4><strong>MR. D.S.P.CHAURASIA</strong></h4>
                            <p>                                
                                PURANA KOPA GANJ NEAR GOVT. HOSPITAL<br>
                                DIST MAU, (U.P.)) - 275305<br>
                                <strong>TIMING</strong> - 9: 30 AM TO 5:30 PM (Monday to Saturday)<br>
                                <strong>FOR ENQUARY</strong> – 8169971843, 8446786625, 8999058337<br>
                                <strong>EMAIL</strong> - <a href="mailTo:upro@riceedu.org">upro@riceedu.org</a><br>
                                <strong>WEBSITE</strong> - <a target="_blank" href="http://www.riceedu.org">www.riceedu.org</a>
                            </p>
                        </div>
                    </div>

                    <div class="col-sm-6">
                        <div class="HighlightSection text-center">
                            <h3>FOUNDER &amp; CHAIRPERSION</h3>
                            <h4><strong>MR.N.K.SAHU</strong></h4>
                            <p>
                                <strong>CONTACT</strong> – 9691057096, 9522223647<br>
                                <strong>EMAIL ID</strong> – <a href="mailTo:director@riceedu.org">director@riceedu.org</a>

                            </p>

                            <h3>CHIEF MANAGING DIRECTOR</h3>
                            <h4><strong>MR.Y.P.CHAUHAN</strong></h4>
                            <p>
                                <strong>CONTACT</strong> – 7000953595, 9522223699<br>
                                <strong>EMAIL ID</strong> – <a href="mailTo:cmd@riceedu.org">cmd@riceedu.org</a>
                            </p>

                            <h3>NATIONAL MARKETING HEAD</h3>
                            <h4><strong>MR.C.K.NAG</strong></h4>
                            <p>
                                <strong>CONTACT</strong> – 9522223691, 9301301666<br>
                                <strong>EMAIL ID</strong> – <a href="mailTo:cknag@riceedu.org">cknag@riceedu.org</a>

                            </p>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            
                <div class="page-content contact-page" style="padding:15px;">
                	

                    <div style="display:none;" id="captchaError"></div>
                    
                    <div class="contactFrm col-sm-6" style="margin:to:30px">
                        <form class="form-horizontal fromstyle" role="form" id="contactform" method="post">
                            <div>
                                <div class="form-group clear fieldRow">
                                    <label class="control-label col-sm-12 mandatory" for="name">Full Name <span>*</span></label>
                                    <div class="col-sm-12  col-height">
                                        <input type="text" class="form-control input-sm" id="name" name="name" placeholder="Name"  />
                                    </div>
                                </div>
                                <div class="form-group clear fieldRow">
                                    <label class="control-label col-sm-12 mandatory" for="emailId">Email Id <span>*</span></label>
                                    <div class="col-sm-12 col-height">
                                        <input type="text" class="form-control input-sm" id="emailId" name="emailId" placeholder="Email Id"  />
                                    </div>
                                </div>
                                <div class="form-group clear fieldRow">
                                    <label class="control-label col-sm-12" for="phone">Phone</label>
                                    <div>
                                        <div class="col-sm-3 col-height"><input type="text" class="form-control input-sm" id="countrycode" name="countrycode" placeholder="Ext." /></div>
                                        <div class="col-sm-9 col-height">
                                            <input type="text" class="form-control input-sm" id="phone" name="phone" placeholder="Phone" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group clear">
                                    <label class="control-label col-sm-12 mandatory" for="msg">Message <span>*</span></label>
                                    <div class="col-sm-12 col-height fieldTxtareaRow">
                                        <textarea class="form-control input-sm textarea" id="msg" name="msg" placeholder="Message"></textarea> 
                                    </div>
                                </div>
                                <div class="form-group text-center clear">
                                	<div class="text-center">
                                    	<?php 
					  require_once('recaptchalib.php');
					  $publickey = "6LennPISAAAAAAwbLk1VSEy4h50Zwk1bOJZgwnYN"; // you got this from the signup page
					  echo recaptcha_get_html($publickey);
					?>
                                    </div>
                                </div>
                                
                                <div style="clear:both;"></div>

                                <div class="form-group">
                                    <div class="align_center col-height"><input type="submit" name="submit" class="btn btn-primary btn-sm" id="submit" value="Submit"></div>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="col-sm-6" style="margin-top:20px;">
                    	<h3><strong>FOR COMPLAIN</strong></h3>
                        <p style="padding-top:10px;"><strong style="font-size:1.5em;">किसी भी प्रकार के शिकायत के लिए EMAIL करें </strong><br>
                        Email: <a href="mailto:complain@riceedu.org">complain@riceedu.org</a></p>
                        
                        <h3>Important</h3>
                        <p>Please note that our office timing are 9: 30 AM TO 5:30 PM LUNCH TIME 1:00
PM TO 1:30 PM (Monday to Saturday )<br>
                        <strong>Note:</strong> Please do not make us any calls before and after the office timing.</p>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--eof homeMid ** footer starts from here-->
        
        <a href="#" class="back-to-top">&nbsp;</a>
<?php include('footer.php'); ?>