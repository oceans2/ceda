<?php
session_start();
date_default_timezone_set('Asia/Calcutta');
define('SERVER','localhost');
define('DBUSER','root');//define(DBUSER,'kritipho_to');
define('DBPASSWORD','');
define('DBNAME','riceedu');
define('ROOT',$_SERVER['DOCUMENT_ROOT'].'/ceda'); //FOR PHP ROOT LINK
//define(ROOT,$_SERVER['DOCUMENT_ROOT']); //FOR PHP ROOT LINK
define('PATH_CEDA','/ceda');
define('PATH_LIBRARIES', ROOT.'/libraries');
define('PATH_JS_LIBRARIES','/ceda');
define('PATH_CSS_LIBRARIES','/ceda');
define('PATH_IMAGE','/ceda/images');
define('PATH_IMAGE_NEW','/ceda/css');
define('PATH_DATA_IMAGE','/ceda/data_images');
define('PATH_DATA_IMAGE_CM','/ceda/cm_image');
define('PATH_DATA_PRESS_IMAGE','/ceda/press_release_image');
define('PATH_DATA_PRESS_IMAGE_Gallery','/ceda/image_gallery');
define("PATH_FONTS",'/ceda/fonts'); // fonts Link
define('PATH_DATA_DOC','/ceda/documents');//documents

define('PATH_ROOT', '');
// for adminpanel
//masters path setup
define('PATH_MASTERS', ROOT.'/adminpanel');
define('PATH_ADMIN_LINK', '/ceda/adminpanel'); //for html link only
define('PATH_ADMIN_INCLUDE',ROOT.'/adminpanel/include');
define('MASTERS_LINK_CONTROL','/ceda/adminpanel/masters');

//RM Panel Section
define("LINK_CONTROL_RM", '/ceda/rmpanel');
define("PATH_RM", ROOT.'/rmpanel');
define("PATH_RM_INCLUDE",PATH_RM.'/include');

//DM Panel Section
define("LINK_CONTROL_DM", '/ceda/dmpanel');
define("PATH_DM", ROOT.'/dmpanel');
define("PATH_DM_INCLUDE",PATH_DM.'/include');

//CM Panel Section
define("LINK_CONTROL_CM", '/ceda/cmpanel');
define("PATH_CM", ROOT.'/cmpanel');
define("PATH_CM_INCLUDE",PATH_CM.'/include');

//Student Panel Section
define("LINK_CONTROL_STUDENT", '/ceda/studentpanel');
define("PATH_STUDENT", ROOT.'/studentpanel');
define("PATH_STUDENT_INCLUDE",PATH_STUDENT.'/include');

/*__________________________________*/
?>