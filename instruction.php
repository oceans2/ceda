<?php include('config.php'); 
include('header.php'); ?>

<!-- Back to Top Script-->
<script>
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.back-to-top').fadeIn(duration);
			} else {
				jQuery('.back-to-top').fadeOut(duration);
			}
		});
		
		jQuery('.back-to-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		});
		
		
	});
</script>
<!-- eof Back to Top Script-->

        <!--eof header ** homeMid starts from here-->
        <div class="container homeMid">
        	<div>
                <div class="page-content container">
                	<h1>Instructions</h1>
                    
                    <div class="container" >
                        <article class="welcome">
                          
                               <h5 style="font-size:2em;">फोटो स्कैन करना </h5>
                               <p>सबसे  पहले आप फोटो के साथ सिग्नेचर को इस प्रकार स्कैन करके JPEG/PNG/JPG फार्मेट के साथ  सिस्टम मे रख लें,<br></p>
                               <p><img src="images/photo-format.jpg" alt=""></p>

                               <h5 style="font-size:2em; margin-top:40px;">फार्म कैसे भरें</h5>
                               <p>अब आप APPLY FOR EXAM पर CLICK करें इसके बाद दिये गए ऑनलाइन फार्म को बिधिवत फ़िल करें ध्यान रहे जब आप फार्म फ़िल करते है तो जानकारी सही भरें ,बाद मे एडिट नहीं होगा ,  ईमेल आईडी,(यदि ईमेल आईडी न हो तो किसी पहचान वाले का सबमिट करना अनिवार्य है ) और मोबाइल नंबर सही फ़िल करें , ऑनलाइन फार्म भरने के बाद आप submit पर क्लिक करे फार्म सबमिट होने के बाद आपके द्वारा फार्म पर दिये गए  मोबाइल और ईमेल आईडी पर एक एसएमएस या मेल प्राप्त  होगा जो की आपका रजिस्ट्रेशन नंबर होगा |</p>

                               <h5 style="font-size:2em; margin-top:40px;">ऑनलाइन फीस / payment कैसे करें</h5>
                               <p>आप make scholarship exam payment पर क्लिक करें मोबाइल या ईमेल आईडी पर प्राप्त रजिस्ट्रेशन आईडी को टाइप करें फिर लॉगिन करें इसके बाद pay by net banking पर क्लिक करें आप पेमेंट करने के लिए debit card , credit card ,internet banking मे से  किसी भी एक option का उपयोग  करके पेमेंट कर सकते हैं  पेमेंट करने के लिए बैंकिंग ऑप्शन का स्टेप बाई  स्टेप फॉलो करें इसके बाद pay now करें फीस process होगा और transaction  successful हुआ तो आपका EXAM फीस पैड होगा अन्यथा transaction fail होने पर 'Scholarship Exam' पेज में जाकर 'Make Scholarship Exam Payment' बटन पर क्लिक करें और आपके मोबाइल पर प्राप्त रजिस्ट्रेशन आई डी डालकर फीस का भुगतान  करें |</p>

                               <h5 style="font-size:2em; margin-top:40px;">ऑफलाइन फॉर्म कैसे भरे </h5>
                               <p>ऑफलाइन एक्जाम आवेदन फार्म भरने के लिए वेब साइट www.riceedu.org मे स्कॉलर्शिप एक्जाम से ऑफलाइन फार्म डौन्लोड करे इसके बाद फार्म को फ़िल करे और राइस सोसाइटी के अकाउंट मे फीस ट्रान्सफर कर फीस रसीद को फार्म मे संलग्न करके हैड ऑफिस एड्रैस पर भेज दें। फॉर्म प्राप्त होने के बाद आपको रजिस्ट्रेशन न. आपके मोबाइल पर प्राप्त होगा |<br><br>
                               <strong style="font-size:1.7em;">ऑफिस एड्रैस है:</strong> <br><br>
                               <strong>RURAL INSTITUTE FOR CAREER & EMPLOYMENT SOCIETY</strong><br>
                               OLD SARANGARH BUS STAND BEHIND KANHA HOSPITAL WARD NO. 31 RAIGARH (C.G.) – 496001 <br>
                               <strong>TIMING</strong> - 9: 30 AM TO 5:30 PM (Monday to Saturday ) <br>
                               <strong>FOR ENQUARY</strong> – 07762222569, 9522223698, 9329057958 <br>
                               <strong>EMAIL</strong> - support@riceedu.org <br>
                               <strong>WEBSITE</strong> - www.riceedu.org</p>

                               <h5 style="font-size:2em; margin-top:40px; color:#ff0000;"><strong>नोट</strong></h5>
                               <p>मोबाइल पर sms द्वारा प्राप्त रजिस्ट्रेशन नंबर द्वारा आप एक्जाम  के लिए प्रवेश पत्र और रिज़ल्ट ऑनलाइन हमारे वैबसाइट www.riceedu।org पर समयानुसार प्राप्त कर सकते है |</p>
                            
                        </article><!--//page-content-->
                        
                       
                        
                    </div>
                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--eof homeMid ** footer starts from here-->
        <a href="#" class="back-to-top">&nbsp;</a>
<?php include('footer.php'); ?>