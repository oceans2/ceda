<?php 
include('config.php'); 
include('header.php'); ?>

<!-- Back to Top Script-->
<script>
	jQuery(document).ready(function() {
		var offset = 220;
		var duration = 500;
		jQuery(window).scroll(function() {
			if (jQuery(this).scrollTop() > offset) {
				jQuery('.back-to-top').fadeIn(duration);
			} else {
				jQuery('.back-to-top').fadeOut(duration);
			}
		});
		
		jQuery('.back-to-top').click(function(event) {
			event.preventDefault();
			jQuery('html, body').animate({scrollTop: 0}, duration);
			return false;
		});
		
		
	});
</script>
<!-- eof Back to Top Script-->

        <!--eof header ** homeMid starts from here-->
        <div class="container homeMid">
        	<div>
                <div class="page-content container">
                	<div class="pageTitle">
                		<div class="col-sm-4">
                			<h1>Sholarship Exam</h1>
                		</div>
                		<div class="col-sm-8 text-right examBtns"><a class="btn btn-info" href="instruction.php">Instruction</a> <a target="_blank" class="btn btn-danger" href="downloads/offiline-scholarship-test-exam-form.pdf">Download Scholarship Exam Form</a> <a class="btn btn-warning" href="scholarship.php">Apply For Exam</a> <a class="btn btn-primary"  href="scholarlogin.php">Make Scholarship Exam Payment</a></div>
                		<div class="clearfix"></div>
                	</div>
                    
                    <div class="container" >
                        <img src="images/1.jpg" alt="">
                        <img src="images/2.jpg" alt="">
                        <img src="images/3.jpg" alt="">
                    </div>
                    
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <!--eof homeMid ** footer starts from here-->
        <a href="#" class="back-to-top">&nbsp;</a>
<?php include('footer.php'); ?>