// JavaScript Document
$(document).ready(function(){
	$("#loading").hide();
	
	///////////////////////////////////
	// Add Student form validation
	////////////////////////////////////
	$("#insertStudent").validate({
		rules: 
		{ 
			fileupload:
			{
				required: true,
			},
			student_sign:
			{
				required: true,
			},
			guardian_sign:
			{
				required: true,
			},
			student_name: 
			{ 
				required: true,
			},
			dob: 
			{ 
				required: true,
			},
			father_name:
			{
				required:true,
			},
			mother_name: 
			{ 
				required: true,
			},
			religion: 
			{ 
				required: true,
			},
			caste: 
			{ 
				required: true,
			},
			aadhaar_no: 
			{ 
				required: true,
			},			
			education:
			{
				required: true,
			},
			course:
			{
				required: true,
			},
			mode:
			{
				required: true,
			},
			postofice:
			{
				 required: true,
			},
			session:
			{
				required: true,
			},
			fee_deposit_detail:
			{
				required: true,
			},
			state:
			{
				required: true,
			},
			district:
			{
				required: true,
			},
			block:
			{
				required: true,
			},
			address:
			{
				required: true,
			},
			pincode:
			{
				required: true,
			},
			contact_no:
			{
				required: true,
				minlength: 10,
				maxlength: 11,
				number:true
			},			
			email:
			{
				required: true,
				email:true
			},
			exam:
			{
				required: true,
			},
			

		},
		messages:
		{
			
		}
	});// eof validation
	
	
	
	
	$("#rmloginfrm").validate({
		rules: 
		{ 
			regid:
			{
				required: true,
			},
		},
		messages:
		{
			
		}
	});// eof validation
	
	
	
	
	
	//////////////////////////////////////////
	$('#rmloginBtn').click(function(){
		
		flag=$("#rmloginfrm").valid();
		if (flag==true)
		{ 
			$("#loading").show();
			var formdata = new FormData();
			formdata.append('type', "scholarlogin");
			formdata.append('regid', $("#regid").val());
			
		$.ajax({
			   type: "POST",
			   url: "scholarcurd.php",
			   data:formdata,
			   success: function(data){ 
			  
				   $("#loading").hide();
				    if((data!=2 )&& (data!=1))
					{
			 window.location.replace("payment/PayUMoney_form.php?id="+data);
					}
					else
					{
						 if(data==1){$("#msg").html(" Your Payment Is Already Paid");}
					      if(data==2){$("#msg").html("Not Exist");}
					}
				  
					  /* if(data==1){$("#msg").html(" Your Payment Is Already Paid");}
					  if(data==2){$("#msg").html("Not Exist");}
					  if((data!=2 )&& (data!=1))
					{
			 window.location.replace("payment/PayUMoney_form.php?id="+data);
					}*/
				             
			   },
			   cache: false,
			   contentType: false,
			   processData: false
			});//eof ajax
			
			
		}
		
		
		
	});
	
	
	
	
	
	
	
	//////////////////////////////////////////
	// on change of state drop down
	//////////////////////////////////////////
	$('#state').change(function(){
					$('#block').html("<option value=''>--Select Type--</option>");
			var formdata = new FormData();
			formdata.append('type', "getDistrict");
			formdata.append('stateId', $("#state").val());
	
			var x;
			$.ajax({
			   type: "POST",
			   url: "scholarcurd.php",
			   data:formdata,
			   success: function(data){
				   $('#district').html(data);
			   },
			   cache: false,
			   contentType: false,
			   processData: false
			});//eof ajax
		
	});
	
	
	//////////////////////////////////////////
	// on change of district drop down
	//////////////////////////////////////////
	$('#district').change(function(){
					$('#block').html(" ");
			var formdata = new FormData();
			formdata.append('type', "getBlocks");
			formdata.append('districtId', $("#district").val());
	
			var x;
			$.ajax({
			   type: "POST",
			   url: "scholarcurd.php",
			   data:formdata,
			   success: function(data){  //alert(data);
				   $('#block').html(data);
			   },
			   cache: false,
			   contentType: false,
			   processData: false
			});//eof ajax
		
	});
	
	
	//////////////////////////////////
	// on click of submit button
	//////////////////////////////////
	$('#submit').click(function(){
		
		flag=$("#insertStudent").valid();
		
		 
		if (flag==true)
		{		
		    
			$("#loading").show();
			
			
    		var gender = $("input[name='gender']:checked").val();
			var formdata = new FormData();
			formdata.append('type', "addStudent");
			formdata.append('furl',$("#furl").val());
			formdata.append('firstname', $("#student_name").val());
			formdata.append('productinfo', $("#productinfo").val());	
			formdata.append('amount',$("#amount").val());	
			formdata.append('key',$("#key").val());
			formdata.append('hash',$("#hash").val());
			formdata.append('txnid',$("#txnid").val());
			formdata.append('service_provider',$("#service_provider").val());
			formdata.append('surl',$("#surl").val());
			formdata.append('exam',$("#exam").val());
			
			formdata.append('file', $('input[id=fileupload]')[0].files[0]);
			formdata.append('student_name', $("#student_name").val());
			formdata.append('dob', $("#dob").val());
			formdata.append('gender', gender);
			formdata.append('father_name', $("#father_name").val());
			formdata.append('mother_name', $("#mother_name").val());			
			formdata.append('caste', $("#caste").val());			
			formdata.append('aadhaar_no', $("#aadhaar_no").val());
			formdata.append('education', $("#education").val());
			formdata.append('course', $("#course").val());
			formdata.append('block', $("#block").val());
			formdata.append('district', $("#district").val());
			formdata.append('state', $("#state").val());
			formdata.append('address', $("#address").val());
			formdata.append('pincode', $("#pincode").val());
			formdata.append('postofice', $("#postofice").val());
			formdata.append('contact_no', $("#contact_no").val());
			formdata.append('phone', $("#contact_no").val());
			formdata.append('email', $("#email").val());	
			
			$.ajax({
			   type: "POST",
			   url: "scholarcurd.php",
			   data:formdata,
			   success: function(data){ 
				   $("#loading").hide();
				  // alert(data);
			 window.location.replace("payment/PayUMoney_form.php?id="+data);
			   },
			   cache: false,
			   contentType: false,
			   processData: false
			});//eof ajax
		
			
		}// eof if condition
		
	});
	
	
	//////////////////////////////////
	// on click of update button
	//////////////////////////////////
	$("#edit").click(function(){
		
		
		flag=$("#editEmployee").valid();
		
		if (flag==true)
		{
			//Check Here Profile Image Uploaded or not
			var image='';
			var imageval='';			
			if($("#fileupload").val().length>0)
				{
					image=$("#fileupload").prop('files')[0];
					imageval=1;
				}
			
			var formdata = new FormData();
			formdata.append('type', "editEmployee");
			
			formdata.append('emp_image', image);
			formdata.append('imageval', imageval);
			formdata.append('emp-img', $('#emp-img').val());
			
			
			formdata.append('emp_id', $("#emp_id").val());
			formdata.append('emp_code', $("#emp_code").val());
			formdata.append('emp_name', $("#emp_name").val());
			formdata.append('designation', $("#designation").val());
			formdata.append('salary', $("#salary").val());
			formdata.append('state', $("#state").val());
			formdata.append('district', $("#district").val());
			formdata.append('block', $("#block").val());				
			formdata.append('address', $("#address").val());
			formdata.append('posting_place', $("#posting_place").val());
			formdata.append('duty_time', $("#duty_time").val());
			formdata.append('Visiting_Date_Place', $("#Visiting_Date_Place").val());
			formdata.append('contact_no', $("#contact_no").val());
			formdata.append('email', $("#email").val());
			
			
			 var x;
			 $.ajax({
			   type: "POST",
			   url: "employee_curd.php",
			   data:formdata,
			   async: false,
			   success: function(data){ //alert(data);
				   x=data;
			   },
			   cache: false,
			   contentType: false,
			   processData: false
			});
			
			if(x==1)
			{
				window.location.replace("index.php");				
			}
		}//eof if condition
	});
	
	//////////////////////////////////
	// on click of delete button
	//////////////////////////////////
	$(".delete").click(function(){
		
		var didConfirm = confirm("Are you sure?");
	    if (didConfirm == true) {
			var emp_id=$(this).attr("id");
			
			$.ajax({
				url:"employee_curd.php",
				type: "POST",
				data: {type:"delete",emp_id:emp_id},
				async:false,
				success: function(data){ //alert(data);
				}
			});
			location.reload();
	    }
	});
	
});//eof of ready function